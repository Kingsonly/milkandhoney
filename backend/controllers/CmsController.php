<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use backend\models\Cms;

/**
 * Site controller
 */
class CmsController extends Controller
{
	public function actionIndex()
	{
		$this->layout = 'dashboardtwo';
		return $this->render('index');
	}
}
