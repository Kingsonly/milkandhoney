<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use backend\models\LoginForm;
use backend\models\BackendProperties;
use backend\models\BackendClients;
use backend\models\BackendConsultant;
use backend\models\BackendUserdb;
use backend\models\Sms;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error','sendsms'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {   
		$this->layout = 'dashboardtwo';
		$propertiesModel = new BackendProperties();
		$clientModel = new BackendClients();
		$consultantModel = new BackendConsultant();
		$userModel = new BackendUserdb();
		// get total numbers off all clients, properties, consultants and users
		$countPropertiesModel = count($propertiesModel->find()->all());
		$countClientModel = count($clientModel->find()->all());
		$countConsultantModel = count($consultantModel->find()->all());
		$countUserModel = count($userModel->find()->all());
		$smsModel = new Sms();
		$totalSmsLeft = $smsModel->totalSmsLeft;
		return $this->render('index',[
			'properties' => $countPropertiesModel,
			'client' => $countClientModel,
			'consultant' => $countConsultantModel,
			'users' => $countUserModel,
			'sms' => $totalSmsLeft,
		]);
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
		
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            $model->password = '';

            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }
	
	function smssend($owneremail,$subacct,$subacctpwd,$sendto,$sender,$message){
        
		/*url =
		"http://www.smslive247.com/http/index.aspx?" . "cmd=sendquickmsg"
		. "&owneremail=" . UrlEncode($owneremail)
		. "&subacct=" . UrlEncode($subacct)
		. "&subacctpwd=" . UrlEncode($subacctpwd) . "&message=" . UrlEncode($message)."&sendto=".UrlEncode($sendto)."&sender=".UrlEncode('kings2');*/
		
		$url =
		"http://www.bulksmsbase.com/components/com_spc/smsapi.php?username=".UrlEncode('kingsonly') . "&password=".UrlEncode('P@55w)rd').
		"&balance=true";
		

		if ($f = @fopen($url, "r")){ 
			$answer = fgets($f, 255);
			if (substr($answer, 0, 1) == "+"){
				echo "SMS to $dnr was successful.";
			} elseif($answer){
				echo $answer;
			} else {
				echo 1;
			}
		} else {
			echo 0;
		}


        
        //send email
    }
	
	public function actionSendsms()
    {
		
        return $this->smssend('kingsonly13c@gmail.com','KINGSONLY','firstoctober','08083430800','mike','test');
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}
