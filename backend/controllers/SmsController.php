<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use backend\models\LoginForm;
use backend\models\Sms;

/**
 * Site controller
 */
class SmsController extends Controller
{

    /**
     * {@inheritdoc}
     */
	
	public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                   
                    [
                        
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            
        ];
    }
    public function actions()
    {
		$this->layout = 'dashboardtwo';
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {   
		
		$model = new Sms();
        return $this->render('index',['model' => $model]);
    }

    
	function smssend($owneremail,$subacct,$subacctpwd,$sendto,$sender,$message){
        
		$url =
		"http://www.bulksmsbase.com/components/com_spc/smsapi.php?username=".UrlEncode('kingsonly') . "&password=".UrlEncode('P@55w)rd')."&sender=".UrlEncode($sender)."&recipient=".UrlEncode($sendto)."&message=".UrlEncode($message)."&msgid=4";
		
		


		if ($f = @fopen($url, "r")){ 
			$answer = fgets($f, 255);
			if (substr($answer, 0, 1) == "+"){
				return "SMS to $dnr was successful.";
			} elseif($answer){
				return $answer;
			} else {
				return 1;
			}
		} else {
			return 0;
		}


        
       
    }
	
	
	
	public function actionSendsms()
    {
		$model = new Sms();
		if($model->load(Yii::$app->request->post())){
			
			$abc = Yii::$app->request->post();
			$sender = $abc['Sms']['sender'];
			$contact = $abc['Sms']['contact'];
			$body = $abc['Sms']['body'];
			return $this->smssend('kingsonly13c@gmail.com','KINGSONLY','firstoctober',$contact,$sender,$body);
		}
        
    }

    
}
