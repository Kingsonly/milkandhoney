<?php

namespace backend\models;

use Yii;
use common\models\CommonClients;

/**
 * This is the model class for table "tbl_clients".
 *
 * @property int $id
 * @property string $rand
 * @property string $username
 * @property string $pswrd
 * @property string $fname
 * @property string $lname
 * @property string $email
 * @property string $phone
 * @property int $gender
 * @property string $address
 * @property string $city
 * @property string $state
 * @property string $country
 * @property string $date
 * @property int $status
 * @property int $consultant_id
 * @property int $paid
 * @property int $property_id
 */
class BackendClients extends CommonClients
{
	
}
