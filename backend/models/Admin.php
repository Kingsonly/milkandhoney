<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "tbl_admin".
 *
 * @property int $id
 * @property string $adminID
 * @property string $fname
 * @property string $lname
 * @property string $phone
 * @property string $email
 * @property string $date
 * @property int $priviledge
 * @property int $status
 */
class Admin extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_admin';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['adminID', 'fname', 'lname', 'phone', 'email', 'date', 'priviledge', 'status'], 'required'],
            [['fname', 'lname'], 'string'],
            [['priviledge', 'status'], 'integer'],
            [['adminID', 'phone', 'date'], 'string', 'max' => 20],
            [['email'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'adminID' => 'Admin ID',
            'fname' => 'Fname',
            'lname' => 'Lname',
            'phone' => 'Phone',
            'email' => 'Email',
            'date' => 'Date',
            'priviledge' => 'Priviledge',
            'status' => 'Status',
        ];
    }
}
