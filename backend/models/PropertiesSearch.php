<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Properties;

/**
 * PropertiesSearch represents the model behind the search form of `backend\models\Properties`.
 */
class PropertiesSearch extends Properties
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['property_id', 'status'], 'integer'],
            [['prand', 'consultant_id', 'property_name', 'description', 'commission_percent', 'document', 'photo', 'file', 'location', 'state', 'property_date', 'type'], 'safe'],
            [['price'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Properties::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'property_id' => $this->property_id,
            'price' => $this->price,
            'property_date' => $this->property_date,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'prand', $this->prand])
            ->andFilterWhere(['like', 'consultant_id', $this->consultant_id])
            ->andFilterWhere(['like', 'property_name', $this->property_name])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'commission_percent', $this->commission_percent])
            ->andFilterWhere(['like', 'document', $this->document])
            ->andFilterWhere(['like', 'photo', $this->photo])
            ->andFilterWhere(['like', 'file', $this->file])
            ->andFilterWhere(['like', 'location', $this->location])
            ->andFilterWhere(['like', 'state', $this->state])
            ->andFilterWhere(['like', 'type', $this->type]);

        return $dataProvider;
    }
}
