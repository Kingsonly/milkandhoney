<?php

namespace backend\models;

use Yii;
use common\models\CommonConsoltantTransactions;

/**
 * This is the model class for table "tbl_consoltant_transactions".
 *
 * @property int $id
 * @property int $consultants_id
 * @property int $amount
 * @property string $date
 * @property int $type
 * @property int $status
 */
class BackendConsoltantTransactions extends CommonConsoltantTransactions
{
  
}
