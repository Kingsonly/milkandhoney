<?php

namespace backend\models;

use Yii;
use common\models\CommonProperties;

/**
 * This is the model class for table "tbl_properties".
 *
 * @property int $property_id
 * @property string $prand
 * @property string $consultant_id
 * @property string $property_name
 * @property string $description
 * @property string $commission_percent
 * @property string $document
 * @property string $photo
 * @property string $file
 * @property string $location
 * @property string $state
 * @property string $price
 * @property string $date
 * @property int $status
 */
class BackendProperties extends CommonProperties
{
   
}
