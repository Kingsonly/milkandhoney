<?php

namespace backend\models;

use Yii;
use common\models\CommonTransactions;

/**
 * This is the model class for table "tbl_transactions".
 *
 * @property int $id
 * @property string $client_id
 * @property string $property_id
 * @property string $consultant_id
 * @property string $amount_paid
 * @property string $payment_date
 * @property int $payment_method
 * @property string $payment_rand
 * @property int $status
 */
class BackendTransactions extends CommonTransactions
{
}
