<?php

namespace backend\models;

use Yii;
use common\models\CommonMessage;

/**
 * This is the model class for table "tbl_message".
 *
 * @property int $id
 * @property string $subject
 * @property string $email_address
 * @property string $group_email
 * @property string $email_content
 * @property string $attachment
 * @property int $status
 * @property string $msg_type
 */
class BackendMessage extends CommonMessage
{

}
