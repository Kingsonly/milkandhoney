<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "tbl_properties".
 *
 * @property int $property_id
 * @property string $prand
 * @property string $consultant_id
 * @property string $property_name
 * @property string $description
 * @property string $commission_percent
 * @property string $document
 * @property string $photo
 * @property string $file
 * @property string $location
 * @property string $state
 * @property string $price
 * @property string $property_date
 * @property int $status
 * @property string $type
 *
 * @property TblPropertyDetails[] $tblPropertyDetails
 */
class Properties extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_properties';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['prand', 'consultant_id', 'property_name', 'description', 'commission_percent', 'document', 'photo', 'file', 'location', 'state', 'price', 'status', 'type'], 'required'],
            [['property_name', 'description', 'document', 'photo', 'file', 'location', 'state'], 'string'],
            [['price'], 'number'],
            [['property_date'], 'safe'],
            [['status'], 'integer'],
            [['prand', 'consultant_id'], 'string', 'max' => 25],
            [['commission_percent'], 'string', 'max' => 15],
            [['type'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'property_id' => 'Property ID',
            'prand' => 'Prand',
            'consultant_id' => 'Consultant ID',
            'property_name' => 'Property Name',
            'description' => 'Description',
            'commission_percent' => 'Commission Percent',
            'document' => 'Document',
            'photo' => 'Photo',
            'file' => 'File',
            'location' => 'Location',
            'state' => 'State',
            'price' => 'Price',
            'property_date' => 'Property Date',
            'status' => 'Status',
            'type' => 'Type',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTblPropertyDetails()
    {
        return $this->hasMany(TblPropertyDetails::className(), ['property_id' => 'property_id']);
    }

    /**
     * {@inheritdoc}
     * @return PropertiesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PropertiesQuery(get_called_class());
    }
}
