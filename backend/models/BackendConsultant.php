<?php

namespace backend\models;

use Yii;
use common\models\CommonConsultant;

/**
 * This is the model class for table "tbl_consultant".
 *
 * @property int $id
 * @property int $user_id
 * @property string $f_name
 * @property string $l_name
 * @property string $dob
 * @property string $phones
 * @property string $pic_path
 * @property string $ref_code
 * @property string $country
 * @property string $state
 * @property string $city
 * @property string $address
 * @property string $gen
 * @property string $bank
 * @property string $acc_name
 * @property string $acc_num
 * @property double $in_account
 * @property string $withdraws
 * @property int $status
 * @property int $gender
 * @property string $father
 * @property int $activate
 * @property int $sms_status
 * @property int $email_status
 */
class BackendConsultant extends CommonConsultant
{
   
}
