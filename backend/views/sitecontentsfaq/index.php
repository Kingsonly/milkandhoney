<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\TblSitecontentsFaqSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'F.A.Q';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tbl-sitecontents-faq-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Update F.A.Q', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        //using options to wrap word to make sure they are contained in a column
        'options' => [
                'style'=>'overflow: auto; word-wrap: break-word;'
        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'area',
            'title',
            'icon',
            'value:ntext',
            //'more details:ntext',
            //'status',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
