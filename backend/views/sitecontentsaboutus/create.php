<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\TblSitecontentsAboutus */

$this->title = 'Create Tbl Sitecontents Aboutus';
$this->params['breadcrumbs'][] = ['label' => 'Tbl Sitecontents Aboutuses', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tbl-sitecontents-aboutus-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
