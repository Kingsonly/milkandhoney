<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\PropertiesSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="properties-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'property_id') ?>

    <?= $form->field($model, 'prand') ?>

    <?= $form->field($model, 'consultant_id') ?>

    <?= $form->field($model, 'property_name') ?>

    <?= $form->field($model, 'description') ?>

    <?php // echo $form->field($model, 'commission_percent') ?>

    <?php // echo $form->field($model, 'document') ?>

    <?php // echo $form->field($model, 'photo') ?>

    <?php // echo $form->field($model, 'file') ?>

    <?php // echo $form->field($model, 'location') ?>

    <?php // echo $form->field($model, 'state') ?>

    <?php // echo $form->field($model, 'price') ?>

    <?php // echo $form->field($model, 'property_date') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'type') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
