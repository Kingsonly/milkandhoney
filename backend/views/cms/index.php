<?
use yii\helpers\Url;
?>
 
<div class="row">

	<div id="page-inner">

 		<div class="row text-center pad-top">
 			<div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
            	<div class="div-square">
                	<a href="<?=Url::to(['/sitecontentsaboutus'])?>" >
 						<i class="fa fa-circle-o-notch fa-5x"></i>
                      	<h4>ABOUT US</h4>
                    </a>
                </div>
            </div>

            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
                <div class="div-square">
                    <a href="<?=Url::to(['/sitecontentsfaq'])?>" >
 						<i class="fa fa-envelope-o fa-5x"></i>
                      	<h4>F.A.Q</h4>
                    </a>
                </div>
            </div>
                  
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
                <div class="div-square">
                    <a href="<?=Url::to(['/sitecontentsservice'])?>" >
 						<i class="fa fa-users fa-5x"></i>
                      	<h4>SERVICES</h4>
                    </a>
                </div>
            </div>
                  
                  
		</div>
                          
</div>

</div>

