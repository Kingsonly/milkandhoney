<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Usermanager */

$this->title = 'Update New Admin User: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Usermanagers', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="usermanager-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
