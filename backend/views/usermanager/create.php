<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Usermanager */

$this->title = 'Create New Admin User';
$this->params['breadcrumbs'][] = ['label' => 'New Admin User', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="usermanager-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
