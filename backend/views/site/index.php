<?
use yii\helpers\Url;
?>
 <!-- Sales Cards  -->
                <!-- ============================================================== -->
                <div class="row">
                    <!-- Column -->
                    <div class="col-md-6 col-lg-2 col-xlg-3">
                        <div class="card card-hover">
                            <div class="box bg-cyan text-center">
                                <h2 class="font-light text-white"><i class="mdi mdi-view-dashboard"><?=$consultant;?></i></h2>
                                <h6 class="text-white">Total Consultants</h6>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <div class="col-md-6 col-lg-4 col-xlg-3">
                        <div class="card card-hover">
                            <div class="box bg-success text-center">
                                <h2 class="font-light text-white"><i class="mdi mdi-chart-areaspline"><?=$sms;?></i></h2>
                                <h6 class="text-white">Total Sms</h6>
                            </div>
                        </div>
                    </div>
                     <!-- Column -->
                    <div class="col-md-6 col-lg-2 col-xlg-3">
                        <div class="card card-hover">
                            <div class="box bg-warning text-center">
                                <h2 class="font-light text-white"><i class="mdi mdi-collage"><?=$client;?></i></h2>
                                <h6 class="text-white">Total Clients</h6>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <div class="col-md-6 col-lg-2 col-xlg-3">
                        <div class="card card-hover">
                            <div class="box bg-danger text-center">
                                <h2 class="font-light text-white"><i class="mdi mdi-border-outside"><?=$properties;?></i></h2>
                                <h6 class="text-white">Total Properties</h6>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <div class="col-md-6 col-lg-2 col-xlg-3">
                        <div class="card card-hover">
                            <div class="box bg-info text-center">
                                <h2 class="font-light text-white"><i class="mdi mdi-arrow-all"><?=$users;?></i></h2>
                                <h6 class="text-white">Total Users</h6>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->

                <!-- ============================================================== -->

<div id="page-inner">

                            <div class="row text-center pad-top">
                  <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
                      <div class="div-square">
                           <a href="<?=Url::to(['/cms'])?>" >
 <i class="fa fa-circle-o-notch fa-5x"></i>
                      <h4>CMS</h4>
                      </a>
                      </div>


                  </div>

                  <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
                      <div class="div-square">
                           <a href="#" >
 <i class="fa fa-envelope-o fa-5x"></i>
                      <h4>EMAIL</h4>
                      </a>
                      </div>


                  </div>
                  <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
                      <div class="div-square">
                           <a href="<?=Url::to(['/sms'])?>" >
 <i class="fa fa-lightbulb-o fa-5x"></i>
                      <h4>SMS</h4>
                      </a>
                      </div>


                  </div>
                  <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
                      <div class="div-square">
                           <a href="#" >
 <i class="fa fa-users fa-5x"></i>
                      <h4>NOTIFICATION
							   </h4>
                      </a>
                      </div>


                  </div>
                  <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
                      <div class="div-square">
                           <a href="<?=Url::to(['/properties'])?>" >
 <i class="fa fa-key fa-5x"></i>
                      <h4>PROPERTY MANAGER</h4>
                      </a>
                      </div>


                  </div>
                  <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
                      <div class="div-square">
                           <a href="#" >
 <i class="fa fa-comments-o fa-5x"></i>
                      <h4>TRANSACTIONS / COMMISSION</h4>
                      </a>
                      </div>


                  </div>
              </div>
                 <!-- /. ROW  -->
                <div class="row text-center pad-top">

                 <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
                      <div class="div-square">
                           <a href="#" >
 <i class="fa fa-clipboard fa-5x"></i>
                      <h4>ORDER</h4>
                      </a>
                      </div>


                  </div>
                  <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
                      <div class="div-square">
                           <a href="#" >
 <i class="fa fa-gear fa-5x"></i>
                      <h4>CLIENT</h4>
                      </a>
                      </div>


                  </div>
                  <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
                      <div class="div-square">
                           <a href="#" >
 <i class="fa fa-wechat fa-5x"></i>
                      <h4>CONSULTANT</h4>
                      </a>
                      </div>


                  </div>
                  <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
                      <div class="div-square">
                           <a href="<?=Url::to(['/usermanager'])?>" >
 <i class="fa fa-bell-o fa-5x"></i>
                      <h4>USER MANAGER </h4>
                      </a>
                      </div>


                  </div>


              </div>



    </div>
