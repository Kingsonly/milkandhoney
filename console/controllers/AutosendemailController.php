<?php
namespace console\controllers;

use yii\console\Controller;
 
use Yii;
use yii\helpers\Url;
 
/**
 * autosendemail controller which is used to send saved emails in the database
 */
class AutosendemailController extends Controller {
	
	
	public function actionIndex()
	{
		Yii::$app->mailqueue->process();
	}
}






