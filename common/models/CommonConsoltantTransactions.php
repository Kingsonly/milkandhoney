<?php

namespace common\models;

use Yii;
use frontend\models\ConsoltantTransactions;

/**
 * This is the model class for table "tbl_consoltant_transactions".
 *
 * @property int $id
 * @property int $consultants_id
 * @property int $amount
 * @property string $date
 * @property int $type
 * @property int $status
 */
class CommonConsoltantTransactions extends ConsoltantTransactions
{
    
}
