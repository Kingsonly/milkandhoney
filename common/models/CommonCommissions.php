<?php

namespace frontend\models;

use Yii;
use frontend\models\Commissions;

/**
 * This is the model class for table "tbl_commissions".
 *
 * @property int $id
 * @property string $property_id
 * @property string $commission_paid
 * @property string $percentage
 * @property string $payment_date
 * @property string $payment_rand
 * @property int $status
 * @property int $consultant_id
 * @property int $client_id
 */
class CommonCommissions extends Commissions
{
    
}
