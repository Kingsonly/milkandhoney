<?
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use frontend\models\UsersContact;
?>
<? $this->title = 'Email and Phone numbers'; ?>
<?php $this->beginBlock('title') ?>
 <?= $this->title;?>
<?php $this->endBlock() ?>
<style>
	.formdisplay{
		display:none;
	}
	
	



#button {
	cursor:pointer;
	margin-top:20px;
	right:0;
	height:40px;
	padding-left:24px;
	padding-right:24px;
	font-family:Arial, Helvetica, sans-serif;
	font-weight:bold;
	font-size:20px;
	color:#FFF;
	text-shadow: 0px -1px 0px #000000;
	-webkit-border-radius:8px;
	border-radius:8px;
	border-top:1px solid #FFF;
	-webkit-box-shadow: 0px 2px 14px #000;
	box-shadow: 0px 2px 14px #000;
	background-color: #62add6;
	background-image:url(https://static.tumblr.com/maopbtg/ZHLmgtok7/button.png);
	background-repeat:repeat-x;
}
#button:active {
	zoom: 1;
	filter: alpha(opacity=80);
	opacity: 0.8;
}
#button:focus {
	zoom: 1;
	filter: alpha(opacity=80);
	opacity: 0.8;
}
	
	
	
.preloader-1 {
  
  width: 66px;
  height: 5px;
  display:none;
}






.preloader-1 .line {
  width: 3px;
  height: 5px;
  background: #fff;
  display: inline-block;
  animation: opacity-1 1000ms infinite ease-in-out;
}

.preloader-2 .line {
  width: 1px;
  height: 12px;
  background: #fff;
  margin: 0 1px;
  display: inline-block;
  animation: opacity-2 1000ms infinite ease-in-out;
}

.preloader-1 .line-1, .preloader-2 .line-1 { animation-delay: 800ms; }
.preloader-1 .line-2, .preloader-2 .line-2 { animation-delay: 600ms; }
.preloader-1 .line-3, .preloader-2 .line-3 { animation-delay: 400ms; }
.preloader-1 .line-4, .preloader-2 .line-4 { animation-delay: 200ms; }
.preloader-1 .line-6, .preloader-2 .line-6 { animation-delay: 200ms; }
.preloader-1 .line-7, .preloader-2 .line-7 { animation-delay: 400ms; }
.preloader-1 .line-8, .preloader-2 .line-8 { animation-delay: 600ms; }
.preloader-1 .line-9, .preloader-2 .line-9 { animation-delay: 800ms; }

@keyframes opacity-1 { 
  0% { 
    opacity: 1;
  }
  50% { 
    opacity: 0;
  }
  100% { 
    opacity: 1;
  }  
}

@keyframes opacity-2 { 
  0% { 
    opacity: 1;
    height: 15px;
  }
  50% { 
    opacity: 0;
    height: 12px;
  }
  100% { 
    opacity: 1;
    height: 15px;
  }  
}

</style>

<div class="row">
	<div class="col-12">
		<div class="card">
			<div class="card-body">
				<h5 class="card-title m-b-0">Emails</h5>
			</div>
			<table class="table table-striped">
				<thead>
					<tr>
					<th scope="col">#</th>
					<th scope="col">Name</th>
					<th scope="col">Email</th>
					<th scope="col">Phone</th>
					<th scope="col">Action</th>
					</tr>
				</thead>
				<tbody>
					<? 
					$a = 1;
					foreach($contactDetails as $value){
						$usersContacts = new UsersContact();
						$model = $usersContacts->findOne($value->id);
					?>
					
							<tr data-id ="<?= $value->id; ?>">
								<?php $form = ActiveForm::begin(['action' => ['sms/sendsms'],'options' => ['class' => 'updatecontact']]); ?>
							  <th scope="row"><?=$a;?></th>
							  <td>
								  <div class="stringdisplay">
									  <?= $value-> name ;?>
									  
								  </div>
								  <div class="formdisplay">
									  
									  <?= $form->field($model, 'name')->textInput(['autofocus' => true,'id' => 'title'])->label(false); ?>
								  </div>
								</td>
							  <td>
								  <div class="stringdisplay">
									  <?= $value-> email;?>	
									  
								  </div>
								  <div class="formdisplay">
									  	
									  <?= $form->field($model, 'email')->textInput(['autofocus' => true,'id' => 'title'])->label(false); ?>
								  </div>
								</td>
							  <td>
								  <div class="stringdisplay">
									  <?= $value-> phone_number?>
									  
								  </div>
								  
								  <div class="formdisplay">
									  
									  <?= $form->field($model, 'phone_number')->textInput(['autofocus' => true,'id' => 'title'])->label(false); ?>
								  </div>
								</td>
							  <td>
								  <div class="formdisplay">
								  <?= Html::submitButton('<span id="sendtext class="senders" ">Send</span><span class="preloader-1">
								  <span>Loading</span>
								  <span class="line line-1"></span>
								  <span class="line line-2"></span>
								  <span class="line line-3"></span>
								  <span class="line line-4"></span>
								  <span class="line line-5"></span>

								  </span>', ['class' => 'btn btn-primary', 'name' => 'contact-button','id' => 'button']); ?>
								  </div>
								  <div class="stringdisplay">
								  <i class="fas fa-pencil-alt"></i>
								  |
								  <i class="far fa-trash-alt" data-del='<?= $value->id; ?>'></i>
								  </div>
							</td>
								<?php ActiveForm::end(); ?>
							</tr>
							<?$a++;?>
					
					<? } ?>                
				</tbody>           
			</table>            
		</div>            
	</div>            
</div>













<?php 
$url = Url::to(['dashboard/users-contact-update']);
$delUrl = Url::to(['dashboard/users-contact-delete']);
$invoiceform = <<<JS
$('.fa-pencil-alt').click(function(){
$(this).parent().parent().parent().find('.stringdisplay').hide()
 $(this).parent().parent().parent().find('.formdisplay').show()
 
})

$('.fa-trash-alt').click(function(){
	delId = $(this).data('del')
    $.post('$delUrl'+'&id='+delId)
    .always(function(result){
	
	res = result.split(" ");
   if(res[0] =='OK'){
   	$('.preloader-1').hide();
	   $('#sendtext').show().text('SMS have been sent and you made use of '+res[1]+' unit');
		
	
	
    }else{
    
		$('#sendtext').show().text('somthing went wrong and sms did not deliver');
		$('.preloader-1').hide();
	
    }
    }).fail(function(){
    console.log('Server Error');
    });
 
 
})

$('.table').DataTable();

$('.updatecontact').on('beforeSubmit', function (e) {
alert(12345)
	$('#sendtext').hide();
	$(this).parent().find('.preloader-1').show();
    var \$form = $(this);
	id = $(this).parent().data('id');
    $.post('$url'+'&id='+id,\$form.serialize())
    .always(function(result){
	
	res = result.split(" ");
   if(res[0] =='OK'){
   	$('.preloader-1').hide();
	   $('#sendtext').show().text('SMS have been sent and you made use of '+res[1]+' unit');
		
	
	
    }else{
    
		$('#sendtext').show().text('somthing went wrong and sms did not deliver');
		$('.preloader-1').hide();
	
    }
    }).fail(function(){
    console.log('Server Error');
    });
	
	setTimeout(function(){ 
	
		$('#sendtext').show().text('Send');
		\$form.reset();

	}, 5000);
    return false;
    
    
});
JS;
 
$this->registerJs($invoiceform);
?>
