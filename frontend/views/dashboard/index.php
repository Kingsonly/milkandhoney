<?
use yii\helpers\Url;
?>
<? $this->title = 'Dashboard'; ?>
<?php $this->beginBlock('title') ?>
<?= $this->title;?>
<?php $this->endBlock() ?>
<div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Sales Cards  -->
                <!-- ============================================================== -->
                <div class="row">
                    <!-- Column -->
                    <div class="col-md-6 col-lg-2 col-xlg-3">
						<a href="<?=Url::to(['/email'])?>">
                        	<div class="card card-hover">
                            <div class="box bg-cyan text-center">
                                <h1 class="font-light text-white"><i class="mdi mdi-email"></i></h1>
                                <h6 class="text-white">email</h6>
                            </div>
                        </div>
						</a>
                    </div>
                    <!-- Column -->
                    <div class="col-md-6 col-lg-2 col-xlg-3">
						<a href="<?=Url::to(['/generation'])?>">
                        	<div class="card card-hover">
                            <div class="box bg-success text-center">
                                <h1 class="font-light text-white"><i class="fa fa-users"></i></h1>
                                <h6 class="text-white">Team Members </h6>
                            </div>
                        </div>
						</a>
                    </div>
					
					<div class="col-md-6 col-lg-2 col-xlg-3">
						<a href="<?=Url::to(['/sms'])?>">
							<div class="card card-hover">
								<div class="box bg-danger text-center">
									<h1 class="font-light text-white"><i class="mdi mdi-message-reply"></i></h1>
									<h6 class="text-white">Sms</h6>
								</div>
							</div>
						</a>
                    </div>
                     <!-- Column -->
                    <div class="col-md-6 col-lg-2 col-xlg-3">
						<a href="<?=Url::to(['/properties'])?>">
                        	<div class="card card-hover">
                            <div class="box bg-warning text-center">
                                <h1 class="font-light text-white"><i class="mdi mdi-home-modern"></i></h1>
                                <h6 class="text-white">Properties</h6>
                            </div>
                        </div>
						</a>
                    </div>
                    <!-- Column -->
                    <div class="col-md-6 col-lg-2 col-xlg-3">
						<a href="<?=Url::to(['/transactions'])?>">
							<div class="card card-hover">
								<div class="box bg-danger text-center">
									<h1 class="font-light text-white"><i class="mdi mdi-cash-multiple"></i></h1>
									<h6 class="text-white">transactions</h6>
								</div>
							</div>
						</a>
                    </div>
                    <!-- Column -->
                    <div class="col-md-6 col-lg-2 col-xlg-3">
						<a href="<?=Url::to(['/clients'])?>">
                        	<div class="card card-hover">
                            <div class="box bg-info text-center">
                                <h1 class="font-light text-white"><i class="mdi mdi-briefcase"></i></h1>
                                <h6 class="text-white">Clients</h6>
                            </div>
                        </div>
						</a>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    
                    <!-- Column -->
                </div>
                <!-- ============================================================== -->
                <!-- Sales chart -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="d-md-flex align-items-center">
                                    <div>
                                        <h4 class="card-title">Mothly Downline Analysis</h4>
                                        <h5 class="card-subtitle">Overview of Latest Month</h5>
                                    </div>
                                </div>
                                <div class="row">
                                    <!-- column -->
                                    <div class="col-lg-9">
                                        <div class="flot-chart">
                                            <div class="flot-chart-content" id="flot-line-chart"></div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="row">
                                            <div class="col-6">
                                                <div class="bg-dark p-10 text-white text-center">
                                                   <i class="mdi mdi-email m-b-5 font-16"></i>
                                                   <h5 class="m-b-0 m-t-5"><?= $totalEmails; ?></h5>
                                                   <small class="font-light">Total email</small>
                                                </div>
                                            </div>
                                             <div class="col-6">
                                                <div class="bg-dark p-10 text-white text-center">
                                                   <i class="fa fa-users m-b-5 font-16"></i>
                                                   <h5 class="m-b-0 m-t-5"><?= $totalGenerations;?></h5>
                                                   <small class="font-light">total team</small>
                                                </div>
                                            </div>
                                            <div class="col-6 m-t-15">
                                                <div class="bg-dark p-10 text-white text-center">
                                                   <i class="mdi mdi-message-reply m-b-5 font-16"></i>
                                                   <h5 class="m-b-0 m-t-5"><?= $totalSms;?></h5>
                                                   <small class="font-light">Total sms</small>
                                                </div>
                                            </div>
                                             <div class="col-6 m-t-15">
                                                <div class="bg-dark p-10 text-white text-center">
                                                   <i class="mdi mdi-home-modern m-b-5 font-16"></i>
                                                   <h5 class="m-b-0 m-t-5"><?= $totalProperties;?></h5>
                                                   <small class="font-light">Total properties</small>
                                                </div>
                                            </div>
                                            <div class="col-6 m-t-15">
                                                <div class="bg-dark p-10 text-white text-center">
                                                   <i class="mdi mdi-cash-multiple m-b-5 font-16"></i>
                                                   <h5 class="m-b-0 m-t-5"><?= $totalCommissions;?></h5>
                                                   <small class="font-light">transactions</small>
                                                </div>
                                            </div>
                                            <div class="col-6 m-t-15">
                                                <div class="bg-dark p-10 text-white text-center">
                                                   <i class="mdi mdi-briefcase m-b-5 font-16"></i>
                                                   <h5 class="m-b-0 m-t-5"><?= $totalClients;?></h5>
                                                   <small class="font-light">clients</small>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- column -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>