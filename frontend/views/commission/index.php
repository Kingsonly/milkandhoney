<?
use yii\helpers\Url;
?>
<? $this->title = 'Commissions'; ?>
<?php $this->beginBlock('title') ?>
<?= $this->title; ?>
<?php $this->endBlock() ?>
<div class="row">
	<div class="col-12">
		<div class="card">
			<div class="card-body">
				<h5 class="card-title m-b-0">Commissions</h5>
			</div>
			<table class="table table-striped">
				<thead>
					<tr>
					<th scope="col">#</th>
					<th scope="col">Client Name</th>
					<th scope="col">Property Name</th>
					<th scope="col">Client Email</th>
					<th scope="col">Client Phone</th>
					<th scope="col">Property price</th>
					<th scope="col">Amount paid by client</th>
					<th scope="col">Commision recieved</th>
					<th scope="col">Action</th>
					</tr>
				</thead>
				<tbody>
					<? 
					$a = 1;
					foreach($commissions as $value){	
					?>
							<tr>
							  <th scope="row"><?=$a;?></th>
							  <td><?=$value->client->fname;?></td>
							  <td><?=$value->client->lname;?></td>
							  <td><?=$value->client->email;?></td>
							  <td><?=$value->client->phones;?></td>
							  <td><?=($value->client->consultant_id == yii::$app->user->identiry->id)?'You':$value->client->user->username;?></td>
							  <td>
								  <? if($value->client->consultant_id == yii::$app->user->identiry->id){ ?>
								  	<a href="<?=Url::to(['/sms','contact'=>'2'.$value->client->phone])?>" title="send sms"><i class="mdi mdi-message-reply"></i></a>
								  <?}?>
								  <? if($value->client->consultant_id == yii::$app->user->identiry->id){ ?>
								  |<a href="<?=Url::to(['/email','id'=>$value->client->email])?>" ><i class="mdi mdi-email"></i></a>|
								  <?}?>
								  <a href="<?=Url::to(['viewgen','id'=>$value->client->id])?>"><i class="mdi mdi-eye"></i></a></td>
							</tr>
							<? $a++;?>

					<? } ?>                
				</tbody>           
			</table>            
		</div>            
	</div>            
</div>

<?php 
	
$commissionJs = <<<JS

	 $('.table').DataTable();
JS;
 
$this->registerJs($commissionJs);
?>
