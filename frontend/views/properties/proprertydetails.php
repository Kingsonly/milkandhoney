<? $this->title = 'Property Details'; ?>
<?php $this->beginBlock('title') ?>
	<?= $this->title; ?>
<?php $this->endBlock() ?>
<section id="single_property">
	
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="property_slider">
					<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">

						<!-- Wrapper for slides -->
						<div class="carousel-inner" role="listbox">
							<div class="item active">
								<img src="img/single_property/1.png" alt="">
							</div>
							<div class="item">
								<img src="img/single_property/2.png" alt="">
							</div>
							<div class="item">
								<img src="img/single_property/3.png" alt="">
							</div>
						</div>

						<!-- Controls -->
						<a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
							<div class="lef_btn">prev</div>
							<span class="sr-only">Previous</span>
						</a>
						<a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
							<div class="right_btn">next</div>
							<span class="sr-only">Next</span>
						</a>
					</div>
				</div>
			</div>
		</div>	
		<div class="row">
			<div class="col-md-8">					
				<div class="row">
					<div class="col-md-12">
						<div class="detail_text">
							<div class="property-text">
								<h4 class="property-title">Luxury Condos Infront of the street of Green Park</h4>
								<span><i class="fa fa-map-marker" aria-hidden="true"></i> 402 Henry Ford Avenue Sand Springs, LA 74063 </span>
								<blockquote>Dis at habitasse viverra nisl. Arcu nec id imperdiet venenatis, lacus sollicitudin in et libero sed senectus ullamcorper conubia velit metus lacinia duis fermentum dictumst ut phasellus aptent tempor lectus.</blockquote>
								<p>Feugiat rhoncus Rhoncus mi, eros consectetuer pretium vivamus potenti sem. Vestibulum quisque neque libero molestie. Arcu ac. Amet dis pulvinar nostra leo posuere per dapibus metus, taciti rutrum tristique nullam suspendisse. Fringilla vitae Parturient risus pretium iaculis blandit condimentum proin nisl risus.</p>
								<p>Pellentesque volutpat mollis dignissim molestie Sed placerat hac, elementum senectus parturient vivamus. Odio. Egestas justo ridiculus erat diam. Lobortis id mollis conubia et varius, justo, velit Ultricies tempus tincidunt viverra feugiat mi. Felis lacus pellentesque amet habitasse vehicula porttitor nisl ullamcorper Porta metus netus. Curae; a torquent condimentum amet Elementum. Rhoncus porttitor scelerisque quam suscipit sapien vitae ad maecenas. Venenatis taciti cum felis enim sem mattis sagittis venenatis nec magnis consequat parturient eu massa rhoncus lorem tristique est taciti conubia semper sodales ultricies integer A placerat etiam lacinia hendrerit ante proin congue erat habitant habitasse curabitur pretium nonummy interdum bibendum.</p>
								<p>Dapibus. Sagittis. Quisque cras tellus pede. Sollicitudin. Ridiculus nec. Mus cras quam. Nonummy. Vulputate sit interdum at, iaculis convallis nullam faucibus ad tempor. Ad aliquet rhoncus urna phasellus parturient pellentesque. Erat porta conubia habitant dolor erat praesent felis orci hymenaeos semper tellus imperdiet rutrum Ut magnis mattis nascetur.</p>
							</div>
						</div>
						<div class="more_information">
							<h4 class="inner-title">More Information</h4>
							<div class="profile_data">
								<ul>
									<li><span>Age :</span> 10 Years</li>
									<li><span>Type :</span> Appartment</li>
									<li><span>Installment Facility :</span> Yes</li>
									<li><span>Insurance :</span> Yes</li>
									<li><span>3rd Party :</span> No</li>
									<li><span>Swiming Pool :</span> Yes</li>
									<li><span>Garden and Field :</span> 2400sqft</li>
									<li><span>Total Floor :</span> 3 Floor</li>
									<li><span>Security :</span> 3 Step Security</li>
									<li><span>Alivator :</span> 2 Large</li>
									<li><span>Dining Capacity :</span> 20 People</li>
									<li><span>Exit :</span> 3 Exit Gate</li>
									<li><span>Fire Place :</span> Yes</li>
									<li><span>Heating System :</span> Floor Heating</li>
								</ul>
							</div>
						</div>
						<div class="single_video">
							<video width="400" controls poster="video/poster.png">
								<source src="video/real_estate.mp4" type="video/mp4">
							</video>
						</div>
						<div class="single_feature">
							<h4 class="inner-title">Features</h4>
							<div class="info-list">
								<ul>
									<li><span><i class="fa fa-check-square-o" aria-hidden="true"></i></span>Fitness Lab and Room</li>
									<li><span><i class="fa fa-check-square-o" aria-hidden="true"></i></span>Swiming Pools</li>
									<li><span><i class="fa fa-check-square-o" aria-hidden="true"></i></span>Parking Facility</li>
									<li><span><i class="fa fa-check-square-o" aria-hidden="true"></i></span>Green Park View</li>
									<li><span><i class="fa fa-check-square-o" aria-hidden="true"></i></span>Playground Include</li>
									<li><span><i class="fa fa-check-square-o" aria-hidden="true"></i></span>Garden</li>
									<li><span><i class="fa fa-check-square-o" aria-hidden="true"></i></span>Kitchen Furniture</li>
									<li><span><i class="fa fa-check-square-o" aria-hidden="true"></i></span>Fire Security</li>
									<li><span><i class="fa fa-check-square-o" aria-hidden="true"></i></span>High Class Door</li>
									<li><span><i class="fa fa-check-square-o" aria-hidden="true"></i></span>Store Room</li>
									<li><span><i class="fa fa-check-square-o" aria-hidden="true"></i></span>Marble Floor</li>
								</ul>
							</div>
						</div>
						<div class="single_map">
							<h4 class="inner-title">Location</h4>
							<div id="map" class="map-canvas"> </div>
						</div>
						<div class="rating-box">
							<h4 class="inner-title">Give a Review</h4>
							<form action="#" method="post" class="rating-form">
								<div class="row">
									<div class="col-md-12 rating-animation">
									  <div class="box-header">Move Mouse for Rating</div>
									  <div class="star-rating">
										  <select id="example-reversed" name="rating" autocomplete="off">
											  <option value="Strongly Disagree">Very Bad Review</option>
											  <option value="Disagree">Bad Review</option>											  
											  <option value="Neither Agree or Disagree" selected="selected">Good Review</option>
											  <option value="Agree">Very Good Quality</option>
											  <option value="Strongly Agree">Excellent Quality</option>
										  </select>
									  </div>
									</div>									
									<div class="form-group col-md-6 col-sm-12">
										<input class="form-control" type="text" name="yourname" Placeholder="Your Name" >
									</div>
									<div class="form-group col-md-6 col-sm-12">
										<input class="form-control" type="text" name="youremail" Placeholder="Your Email" >
									</div>
									<div class="form-group col-md-12 col-sm-12">
										<textarea class="ratingcomments" name="ratingcomments" placeholder="Your Comments"></textarea>
									</div>
									<div class="form-group col-md-12 col-sm-12">
										<input id="send" class="btn btn-default" value="Send" type="submit">
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>


			</div>
			<div class="col-md-4">
				<div class="property_sidebar">
					<div class="property_listing sidebar-widget">
						<h4 class="inner-title">Property Summary</h4>
						<div class="profile_data">
							<ul>
								<li><span>Property ID :</span> LA459C27</li>
								<li><span>Listing Type :</span> For Sale</li>
								<li><span>Property Type :</span> Condos</li>
								<li><span>Price :</span> $375,000</li>
								<li><span>Area :</span> 6500 sqft</li>
								<li><span>Bedroom :</span> 5</li>
								<li><span>Bathroom :</span> 4</li>
								<li><span>Car Garage :</span> Yes ( 5 Capacity )</li>
								<li><span>Swimming :</span> Yes ( 1 Large )</li>
								<li><span>Garden :</span> Yes</li>
								<li><span>Rating :</span>
									<i class="fa fa-star" aria-hidden="true"></i>
									<i class="fa fa-star" aria-hidden="true"></i>
									<i class="fa fa-star" aria-hidden="true"></i>
									<i class="fa fa-star" aria-hidden="true"></i>
									<i class="fa fa-star-o" aria-hidden="true"></i>
									(12)
								</li>
							</ul>
						</div>
					</div>
					<div class="contact_agent sidebar-widget">
						<div class="agent_details">
							<div class="author_img">
								<img src="img/testimonial/2.png" alt="">
							</div>
							<div class="agent_info">
								<h5 class="author_name">Rebecca D. Nagy</h5>
								<span>+( 81 ) 84 538 91231</span>
							</div>
							<form class="message_agent" action="#" method="post">
								<div class="input-group">
									<input type="text" class="form-control" name="name" placeholder="Your Name">
								</div>
								<div class="input-group">
									<input type="text" class="form-control" name="email" placeholder="Your Email">
								</div>
								<div class="input-group">
									<textarea class="form-control" name="message" placeholder="Message"></textarea>
								</div>
								<div class="input-group">
									<button type="submit" class="btn btn-default" name="submit">Send</button>
								</div>
							</form>
						</div>
					</div>
					<div class="featured_sidebar sidebar-widget">
						<h4 class="widget-title">Featured Property</h4>
						<div class="slide_featured">
							<div class="item">
								<div class="thumb">
									<div class="sidebar_img">
										<img src="img/property_grid/property_grid-4.png" alt="">
										<div class="sale_btn">sale</div>
										<div class="quantity">
											<ul>
												<li><span>Area</span>1600 Sqft</li>
												<li><span>Rooms</span>7</li>
												<li><span>Beds</span>4</li>
												<li><span>Baths</span>3</li>
												<li><span>Garage</span>1</li>
											</ul>
										</div>
									</div>
									<div class="property-text">
										<a href="#"><h6 class="property-title">New Developed Condos</h6></a>
										<div class="property_price">$150,000</div>
									</div>
								</div>
							</div>
							<div class="item">
								<div class="thumb">
									<div class="sidebar_img">
										<img src="img/property_grid/property_grid-5.png" alt="">
										<div class="sale_btn">sale</div>
										<div class="quantity">
											<ul>
												<li><span>Area</span>1600 Sqft</li>
												<li><span>Rooms</span>7</li>
												<li><span>Beds</span>4</li>
												<li><span>Baths</span>3</li>
												<li><span>Garage</span>1</li>
											</ul>
										</div>
									</div>
									<div class="property-text">
										<a href="#"><h6 class="property-title">New Developed Condos</h6></a>
										<div class="property_price">$150,000</div>
									</div>
								</div>
							</div>
							<div class="item">
								<div class="thumb">
									<div class="sidebar_img">
										<img src="img/property_grid/property_grid-6.png" alt="">
										<div class="sale_btn">sale</div>
										<div class="quantity">
											<ul>
												<li><span>Area</span>1600 Sqft</li>
												<li><span>Rooms</span>7</li>
												<li><span>Beds</span>4</li>
												<li><span>Baths</span>3</li>
												<li><span>Garage</span>1</li>
											</ul>
										</div>
									</div>
									<div class="property-text">
										<a href="#"><h6 class="property-title">New Developed Condos</h6></a>
										<div class="property_price">$150,000</div>
									</div>
								</div>
							</div>
							<div class="item">
								<div class="thumb">
									<div class="sidebar_img">
										<img src="img/property_grid/property_grid-7.png" alt="">
										<div class="sale_btn">sale</div>
										<div class="quantity">
											<ul>
												<li><span>Area</span>1600 Sqft</li>
												<li><span>Rooms</span>7</li>
												<li><span>Beds</span>4</li>
												<li><span>Baths</span>3</li>
												<li><span>Garage</span>1</li>
											</ul>
										</div>
									</div>
									<div class="property-text">
										<a href="#"><h6 class="property-title">New Developed Condos</h6></a>
										<div class="property_price">$150,000</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>
</section>
		<!-- Single Property End --> 
		
		