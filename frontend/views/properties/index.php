<?
use yii\helpers\Url;
use yii\helpers\Html;
?>
<? $this->title = 'Properties'; ?>
<?php $this->beginBlock('title') ?>
	<?= $this->title;?>
<?php $this->endBlock() ?>
<style>
.dropbtn {
    background-color: #4CAF50;
    color: white;
    padding: 16px;
    font-size: 16px;
    border: none;
}

.dropup {
    position: relative;
    display: inline-block;
}

.dropup-content {
    display: none;
    position: absolute;
    background-color: #f1f1f1;
    min-width: 160px;
    bottom: 50px;
    z-index: 1;
}

.dropup-content a {
    color: black;
    padding: 12px 16px;
    text-decoration: none;
    display: block;
}

.dropup-content a:hover {background-color: #ccc}

.dropup:hover .dropup-content {
    display: block;
}

.dropup:hover .dropbtn {
    background-color: #3e8e41;
}
</style>
<section id="recent_property">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="title-row">
							<h3 class="section_title_blue"><span>Properties</span></h3>
						</div>
						
					</div>
				</div>
				<div class="row">
					<? if(empty($properties)){?>
					there are no properties
					<? }else{?>
					<? foreach($properties as $property){ ?>
					
					<div class="col-md-4 col-sm-6">
						<div class="property_grid">
							<div class="img_area">
								<div class="sale_btn"><?=$property->type?></div>
								<a href="<?=Url::to(['property-details','id' => $property['property_id']]) ?>"><?= Html::img('@web/images/property/'.$property->photo.'', ['alt' => 'My logo']) ?></a>
								<div class="sale_amount"><?=$property->property_date;?></div>
								<div class="hover_property">
									<ul>
										<li><a href="#"><i class="fa fa-search" aria-hidden="true"></i></a></li>
										<li><a href="#"><i class="fa fa-link" aria-hidden="true"></i></a></li>
									</ul>
								</div>
							</div>
							<div class="property-text"> 
								<a href="<?=Url::to(['property-details','id' => $property['property_id']]) ?>">
									<h5 class="property-title"><?=$property->property_name?></h5>
								</a> <span><i class="fa fa-map-marker" aria-hidden="true"></i> <?=$property->location?>( <?=$property->state?> )</span>
								<div class="quantity">
									<ul>
										<? foreach($property->propertyAddon as $addons){ ?>
										<li><span><?=$addons->title;?></span><?=$addons->description;?></li>
										<?}?>
									</ul>
								</div>
							</div>
							<div class="bed_area">
								<ul>
									<li><?=$property->price?></li>
									
									<li class="flat-icon dropup" style="width:50%;"><a href="#" ><i class=" flaticon-connections"></i>
									</a>
									  <div class="dropup-content">
										<a href="<?=Url::to(['/email','id' => $property['property_id']])?>">Email</a>
										<a href="<?=Url::to(['/sms','id' => $property['property_id']])?>">SMS</a>
										
									  </div>

									</li>
									
								</ul>
								
							</div>
							
							<div class="bed_area">
								
								<ul>
									<li>Total referal view: <?=count($property['propertyViewCount'])?></li>
									
									
								</ul>
							</div>
						</div>
					</div>
					<?}}?>
				</div>
			</div>
		</section>


		<!-- Recent Property End --> 