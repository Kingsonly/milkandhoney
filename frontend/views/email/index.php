<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use yii\helpers\ArrayHelper;

$this->title = 'Email';
$this->params['breadcrumbs'][] = $this->title;
?>

<?php $this->beginBlock('title') ?>
<?= $this->title;?>
<?php $this->endBlock() ?>
<div class="row">
	<div style="margin:0 auto; width:80%;">
			<?php $form = ActiveForm::begin(['id' => 'emailform','action'=>['email/sendemail']]); ?>
										
									
									
									<div class="form-group col-md-12 col-sm-12">
                <?= $form->field($model, 'email')->textInput(['id'=> 'email','value'=>($id === 0)?'':$id,'readonly' => ($id === 0)?false:true]) ?>
									</div>
									<div class="form-group col-md-12 col-sm-12">
                <?= $form->field($model, 'subject')->textInput(['id'=> 'subject']) ?>
									</div>
	
				<div class="form-group col-md-12 col-sm-12">
					
					
					<?=$form->field($model, 'others')
						->dropdownList([
							'alldownline' => 'All Downlines', 
							'directdownline' => 'Direct Downlines',
							'savecontact' => 'Saved Contacts'
						],['prompt'=>'Add Other Contacts'] 
					); ?>

				</div>
                

                
			<div class="form-group col-md-12 col-sm-12">

				<?= $form->field($model, 'body')->textarea(['rows' => 6, 'id' => 'message', 'value' => ($id >0)?'we have a new property you might like please click on the link to view property details . http://localhost/milkhoney/frontend/web/index.php?r=site/propertydetails&id='.$id.'&ref='.yii::$app->user->identity->id:'', 'readonly' => ($id > 0)?true:false ]); ?>
			</div>
					<div class="form-group col-md-12 col-sm-12">
                <?= $form->field($model, 'verifyCode')->widget(Captcha::className(), [
                    'template' => '<div class="row"><div class="col-lg-3">{image}</div><div class="col-lg-6">{input}</div></div>',
                ]) ?>
									</div>

              <div class="form-group col-md-12 col-sm-12">
                    <?= Html::submitButton('Submit', ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
                </div>

            <?php ActiveForm::end(); ?>
									
									
									
									
									<div class="col-md-12">
										<div class="error-handel">
											<div id="success">Your email sent Successfully, Thank you.</div>
											<div id="error"> Error occurred while sending email. Please try again later.</div>
										</div>
									</div>
	</div>
	
</div>




<?php 
	
$emailform = <<<JS

$('#emailform').on('beforeSubmit', function (e) {
	$('#sendtext').hide();
	$('.preloader-1').show();
    var \$form = $(this);
	
    $.post(\$form.attr('action'),\$form.serialize())
    .always(function(result){
	
	res = result.split(" ");
   if(res[0] =='OK'){
   	$('.preloader-1').hide();
	   $('#sendtext').show().text('SMS have been sent and you made use of '+res[1]+' unit');
		
	
	
    }else{
    
		$('#sendtext').show().text('somthing went wrong and sms did not deliver');
		$('.preloader-1').hide();
	
    }
    }).fail(function(){
    console.log('Server Error');
    });
	
	setTimeout(function(){ 
	
		$('#sendtext').show().text('Send');
		\$form.reset();

	}, 5000);
    return false;
    
    
});
JS;
 
$this->registerJs($emailform);
?>
