<?
use yii\helpers\Url;
?>
<? $this->title = 'Downlines'; ?>
<?php $this->beginBlock('title') ?>
 <?= $this->title;?>
<?php $this->endBlock() ?>
<div class="accordion">
	<div class="panel-group" id="accordion1" role="tablist" aria-multiselectable="true">
		<div class="panel panel-default accordion_panel">
			<div class="panel-heading accordian_title" role="tab" id="heading1">
				<div class="panel-title">
					<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse1" aria-expanded="true" aria-controls="collapse1">

						Direct referals 
						<span><i class="fa fa-angle-right" aria-hidden="true"></i></span>
					</a>
				</div>
			</div>
			<div id="collapse1" class="panel-collapse collapse  " role="tabpanel" aria-labelledby="heading1">
				<div class="panel-body accordian_para">
					<p>
						<div class="row">
	<div class="col-12">
		<div class="card">
			<div class="card-body">
				<h5 class="card-title m-b-0">Direct referals</h5>
			</div>
			<table class="table table-striped">
				<thead>
					<tr>
					<th scope="col">#</th>
					<th scope="col">Username</th>
					<th scope="col">Email</th>
					<th scope="col">Phone</th>
					<th scope="col">Action</th>
					</tr>
				</thead>
				<tbody>
					<? 
					$a = 1;
					foreach($getAllDownline as $value){
						$val=$value['gen'];
						$gen=explode(',', $val);
						$keys = array_search(yii::$app->user->identity->id, $gen);
						
						if($keys === 0){
						//	return var_dump($getAllDownline);
					?>
							<tr>
							  <th scope="row"><?=$a;?></th>
							  <td><?=$value->user['username'];?></td>
							  <td><?=$value->user['email'];?></td>
							  <td><?=$value->phones;?></td>
							  <td>
								  <a href="<?=Url::to(['/sms','contact'=>'2'.$value->phones])?>" title="send sms"><i class="mdi mdi-message-reply"></i></a>
								  |<a href="<?=Url::to(['/email','id'=>$value->user['email']])?>" ><i class="mdi mdi-email"></i></a>|
								  <a href="<?=Url::to(['viewgen','id'=>$value->user['id']])?>"><i class="mdi mdi-eye"></i></a></td>
							</tr>
							<? $a++;?>

					<? }else{ continue;}} ?>                
				</tbody>           
			</table>            
		</div>            
	</div>            
</div>
						</p>
				</div>
			</div>
		</div>
		
	</div>
	
	<br/>
	
	<div class="panel-group" id="accordion2" role="tablist" aria-multiselectable="true">
		<div class="panel panel-default accordion_panel">
			<div class="panel-heading accordian_title" role="tab" id="heading2">
				<div class="panel-title">
					<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse2" aria-expanded="true" aria-controls="collapse2">

						Second Generation
						<span><i class="fa fa-angle-right" aria-hidden="true"></i></span>
					</a>
				</div>
			</div>
			<div id="collapse2" class="panel-collapse collapse  " role="tabpanel" aria-labelledby="heading2">
				<div class="panel-body accordian_para">
					<p>
						<div class="row">
	<div class="col-12">
		<div class="card">
			<div class="card-body">
				<h5 class="card-title m-b-0">Second Generation</h5>
			</div>
			<table class="table table-striped">
				<thead>
					<tr>
					<th scope="col">#</th>
					<th scope="col">Username</th>
					<th scope="col">Email</th>
					<th scope="col">Phone</th>
					<th scope="col">Action</th>
					</tr>
				</thead>
				<tbody>
					<? 
					$a = 1;
					foreach($getAllDownline as $value){
						$val=$value['gen'];
						$gen=explode(',', $val);
						$keys = array_search(yii::$app->user->identity->id, $gen);
						
						if($keys === 1){	
					?>
							<tr>
							  <th scope="row"><?=$a;?></th>
							  <td><?=$value->user->username;?></td>
							  <td><?=$value->user->email;?></td>
							  <td><?=$value->phones;?></td>
							  <td>
								  <a href="<?=Url::to(['/sms','contact'=>'2'.$value->phones])?>"><i class="mdi mdi-message-reply"></i></a>
								  |<a href="<?=Url::to(['/email','id'=>$value->user->email])?>" ><i class="mdi mdi-email"></i></a>|
								  <a href="<?=Url::to(['viewgen','id'=>$value->user->id])?>"><i class="mdi mdi-eye"></i></a></td>
							</tr>
							<?$a++;?>

					<? }else{ continue;}} ?>                
				</tbody>           
			</table>            
		</div>            
	</div>            
</div>
						</p>
				</div>
			</div>
		</div>
		
	</div>
	<br/>
	
	<div class="panel-group" id="accordion3" role="tablist" aria-multiselectable="true">
		<div class="panel panel-default accordion_panel">
			<div class="panel-heading accordian_title" role="tab" id="heading3">
				<div class="panel-title">
					<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse3" aria-expanded="true" aria-controls="collapse3">

						Third Generations
						<span><i class="fa fa-angle-right" aria-hidden="true"></i></span>
					</a>
				</div>
			</div>
			<div id="collapse3" class="panel-collapse collapse  " role="tabpanel" aria-labelledby="heading3">
				<div class="panel-body accordian_para">
					<p>
						<div class="row">
	<div class="col-12">
		<div class="card">
			<div class="card-body">
				<h5 class="card-title m-b-0">Third Generations</h5>
			</div>
			<table class="table table-striped">
				<thead>
					<tr>
					<th scope="col">#</th>
					<th scope="col">Username</th>
					<th scope="col">Email</th>
					<th scope="col">Phone</th>
					<th scope="col">Action</th>
					</tr>
				</thead>
				<tbody>
					<? 
					$a = 1;
					foreach($getAllDownline as $value){
						$val=$value['gen'];
						$gen=explode(',', $val);
						$keys = array_search(yii::$app->user->identity->id, $gen);
						
						if($keys === 2){	
					?>
							<tr>
							  <th scope="row"><?=$a;?></th>
							  <td><?=$value->user->username;?></td>
							  <td><?=$value->user->email;?></td>
							  <td><?=$value->phones;?></td>
							  <td>
								  <a href="<?=Url::to(['/sms','contact'=>'2'.$value->phones])?>"><i class="mdi mdi-message-reply"></i></a>
								  |<a href="<?=Url::to(['/email','id'=>$value->user->email])?>" ><i class="mdi mdi-email"></i></a>|
								  <a href="<?=Url::to(['viewgen','id'=>$value->user->id])?>"><i class="mdi mdi-eye"></i></a></td>
							</tr>
							<?$a++;?>
					<? }else{ continue;}} ?>                
				</tbody>           
			</table>            
		</div>            
	</div>            
</div>
						</p>
				</div>
			</div>
		</div>
		
	</div>
	<br/>
	
	<div class="panel-group" id="accordion4" role="tablist" aria-multiselectable="true">
		<div class="panel panel-default accordion_panel">
			<div class="panel-heading accordian_title" role="tab" id="heading4">
				<div class="panel-title">
					<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse4" aria-expanded="true" aria-controls="collapse4">

						Forth Generation
						<span><i class="fa fa-angle-right" aria-hidden="true"></i></span>
					</a>
				</div>
			</div>
			<div id="collapse4" class="panel-collapse collapse  " role="tabpanel" aria-labelledby="heading4">
				<div class="panel-body accordian_para">
					<p>
						<div class="row">
	<div class="col-12">
		<div class="card">
			<div class="card-body">
				<h5 class="card-title m-b-0">Forth Generation</h5>
			</div>
			<table class="table table-striped">
				<thead>
					<tr>
					<th scope="col">#</th>
					<th scope="col">Username</th>
					<th scope="col">Email</th>
					<th scope="col">Phone</th>
					<th scope="col">Action</th>
					</tr>
				</thead>
				<tbody>
					<? 
					$a=1;
					foreach($getAllDownline as $value){
						$val=$value['gen'];
						$gen=explode(',', $val);
						$keys = array_search(yii::$app->user->identity->id, $gen);
						
						if($keys === 3){	
					?>
							<tr>
							  <th scope="row"><?=$a;?></th>
							  <td><?=$value->user->username;?></td>
							  <td><?=$value->user->email;?></td>
							  <td><?=$value->phones;?></td>
							  <td>
								  <a href="<?=Url::to(['/sms','contact'=>'2'.$value->phones])?>"><i class="mdi mdi-message-reply"></i></a>
								  |<a href="<?=Url::to(['/email','id'=>$value->user->email])?>" ><i class="mdi mdi-email"></i></a>|
								  <a href="<?=Url::to(['viewgen','id'=>$value->user->id])?>"><i class="mdi mdi-eye"></i></a></td>
							</tr>
							<?$a++;?>
					<? }else{ continue;}} ?>                
				</tbody>           
			</table>            
		</div>            
	</div>            
</div>
						</p>
				</div>
			</div>
		</div>
		
	</div>
	<br/>
	
	<div class="panel-group" id="accordion5" role="tablist" aria-multiselectable="true">
		<div class="panel panel-default accordion_panel">
			<div class="panel-heading accordian_title" role="tab" id="heading5">
				<div class="panel-title">
					<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse5" aria-expanded="true" aria-controls="collapse5">

						Fifth Generation
						<span><i class="fa fa-angle-right" aria-hidden="true"></i></span>
					</a>
				</div>
			</div>
			<div id="collapse5" class="panel-collapse collapse  " role="tabpanel" aria-labelledby="heading5">
				<div class="panel-body accordian_para">
					<p>
						<div class="row">
	<div class="col-12">
		<div class="card">
			<div class="card-body">
				<h5 class="card-title m-b-0">Fifth Generation</h5>
			</div>
			<table class="table table-striped">
				<thead>
					<tr>
					<th scope="col">#</th>
					<th scope="col">Username</th>
					<th scope="col">Email</th>
					<th scope="col">Phone</th>
					<th scope="col">Action</th>
					</tr>
				</thead>
				<tbody>
					<? 
					$a =1;
					foreach($getAllDownline as $value){
						$val=$value['gen'];
						$gen=explode(',', $val);
						$keys = array_search(yii::$app->user->identity->id, $gen);
						
						if($keys === 4){	
					?>
							<tr>
							  <th scope="row"><?=$a;?></th>
							  <td><?=$value->user->username;?></td>
							  <td><?=$value->user->email;?></td>
							  <td><?=$value->phones;?></td>
							  <td>
								  <a href="<?=Url::to(['/sms','contact'=>'2'.$value->phones])?>"><i class="mdi mdi-message-reply"></i></a>
								  |<a href="<?=Url::to(['/email','id'=>$value->user->email])?>" ><i class="mdi mdi-email"></i></a>|
								  <a href="<?=Url::to(['viewgen','id'=>$value->user->id])?>"><i class="mdi mdi-eye"></i></a></td>
							</tr>
							<?$a++;?>
					<? }else{ continue;}} ?>                
				</tbody>           
			</table>            
		</div>            
	</div>            
</div>
						</p>
				</div>
			</div>
		</div>
		
	</div>
	<br/>
	
</div>












<?php 
	
$generationJs = <<<JS

	 $('.table').DataTable();
JS;
 
$this->registerJs($generationJs);
?>
