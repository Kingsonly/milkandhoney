<?
use yii\helpers\Url;
?>
<? $this->title = 'Downline Details'; ?>
<?php $this->beginBlock('title') ?>
<?= $this->title;?>
<?php $this->endBlock() ?>
<div class="row">
	<div class="col-6">
		<div class="card">
			
			    display statistics and other details 
		</div>            
	</div>      
	<div class="col-6">
		<div class="card">
			
			  display image and name    
		</div>            
	</div>            
</div>


<div class="row">
	<div class="col-12">
		<div class="card">
			<div class="card-body">
				<h5 class="card-title m-b-0">Direct referals</h5>
			</div>
			<table class="table table-striped">
				<thead>
					<tr>
					<th scope="col">#</th>
					<th scope="col">Username</th>
					<th scope="col">Email</th>
					<th scope="col">Phone</th>
					<th scope="col">Action</th>
					</tr>
				</thead>
				<tbody>
					<? 
					$a = 1;
					foreach($getAllDownline as $value){
						$val=$value['gen'];
						$gen=explode(',', $val);
						$keys = array_search($id, $gen);
						
						if($keys === 0){	
					?>
							<tr>
							  <th scope="row"><?=$a;?></th>
							  <td><?=$value->user->username;?></td>
							  <td><?=$value->user->email;?></td>
							  <td><?=$value->phones;?></td>
							  <td>
								  <a href="<?=Url::to(['/sms','contact'=>'2'.$value->phones])?>"><i class="mdi mdi-message-reply"></i></a>
								  |<a href="<?=Url::to(['/email','id'=>$value->user->email])?>" ><i class="mdi mdi-email"></i></a>|
								  <a href="<?=Url::to(['viewgen','id'=>$value->user->id])?>"><i class="mdi mdi-eye"></i></a></td>
							</tr>
							<? $a++;?>

					<? }else{ continue;}} ?>                
				</tbody>           
			</table>            
		</div>            
	</div>            
</div>

<div class="row">
	<div class="col-12">
		<div class="card">
			<div class="card-body">
				<h5 class="card-title m-b-0">Direct referals</h5>
			</div>
			<table class="table table-striped">
				<thead>
					<tr>
					<th scope="col">#</th>
					<th scope="col">Username</th>
					<th scope="col">Email</th>
					<th scope="col">Phone</th>
					<th scope="col">Action</th>
					</tr>
				</thead>
				<tbody>
					<? 
					$a = 1;
					foreach($getAllDownline as $value){
						$val=$value['gen'];
						$gen=explode(',', $val);
						$keys = array_search($id, $gen);
						
						if($keys === 1){	
					?>
							<tr>
							  <th scope="row"><?=$a;?></th>
							  <td><?=$value->user->username;?></td>
							  <td><?=$value->user->email;?></td>
							  <td><?=$value->phones;?></td>
							  <td>
								  <a href="<?=Url::to(['/sms','contact'=>'2'.$value->phones])?>"><i class="mdi mdi-message-reply"></i></a>
								  |<a href="<?=Url::to(['/email','id'=>$value->user->email])?>" ><i class="mdi mdi-email"></i></a>|
								  <a href="<?=Url::to(['viewgen','id'=>$value->user->id])?>"><i class="mdi mdi-eye"></i></a></td>
							</tr>
							<?$a++;?>

					<? }else{ continue;}} ?>                
				</tbody>           
			</table>            
		</div>            
	</div>            
</div>

<div class="row">
	<div class="col-12">
		<div class="card">
			<div class="card-body">
				<h5 class="card-title m-b-0">Direct referals</h5>
			</div>
			<table class="table table-striped">
				<thead>
					<tr>
					<th scope="col">#</th>
					<th scope="col">Username</th>
					<th scope="col">Email</th>
					<th scope="col">Phone</th>
					<th scope="col">Action</th>
					</tr>
				</thead>
				<tbody>
					<? 
					$a = 1;
					foreach($getAllDownline as $value){
						$val=$value['gen'];
						$gen=explode(',', $val);
						$keys = array_search($id, $gen);
						
						if($keys === 2){	
					?>
							<tr>
							  <th scope="row"><?=$a;?></th>
							  <td><?=$value->user->username;?></td>
							  <td><?=$value->user->email;?></td>
							  <td><?=$value->phones;?></td>
							  <td>
								  <a href="<?=Url::to(['/sms','contact'=>'2'.$value->phones])?>"><i class="mdi mdi-message-reply"></i></a>
								  |<a href="<?=Url::to(['/email','id'=>$value->user->email])?>" ><i class="mdi mdi-email"></i></a>|
								  <a href="<?=Url::to(['viewgen','id'=>$value->user->id])?>"><i class="mdi mdi-eye"></i></a></td>
							</tr>
							<?$a++;?>
					<? }else{ continue;}} ?>                
				</tbody>           
			</table>            
		</div>            
	</div>            
</div>

<div class="row">
	<div class="col-12">
		<div class="card">
			<div class="card-body">
				<h5 class="card-title m-b-0">Direct referals</h5>
			</div>
			<table class="table table-striped">
				<thead>
					<tr>
					<th scope="col">#</th>
					<th scope="col">Username</th>
					<th scope="col">Email</th>
					<th scope="col">Phone</th>
					<th scope="col">Action</th>
					</tr>
				</thead>
				<tbody>
					<? 
					$a=1;
					foreach($getAllDownline as $value){
						$val=$value['gen'];
						$gen=explode(',', $val);
						$keys = array_search($id, $gen);
						
						if($keys === 3){	
					?>
							<tr>
							  <th scope="row"><?=$a;?></th>
							  <td><?=$value->user->username;?></td>
							  <td><?=$value->user->email;?></td>
							  <td><?=$value->phones;?></td>
							  <td>
								  <a href="<?=Url::to(['/sms','contact'=>'2'.$value->phones])?>"><i class="mdi mdi-message-reply"></i></a>
								  |<a href="<?=Url::to(['/email','id'=>$value->user->email])?>" ><i class="mdi mdi-email"></i></a>|
								  <a href="<?=Url::to(['viewgen','id'=>$value->user->id])?>"><i class="mdi mdi-eye"></i></a></td>
							</tr>
							<?$a++;?>
					<? }else{ continue;}} ?>                
				</tbody>           
			</table>            
		</div>            
	</div>            
</div>

<div class="row">
	<div class="col-12">
		<div class="card">
			<div class="card-body">
				<h5 class="card-title m-b-0">Direct referals</h5>
			</div>
			<table class="table table-striped">
				<thead>
					<tr>
					<th scope="col">#</th>
					<th scope="col">Username</th>
					<th scope="col">Email</th>
					<th scope="col">Phone</th>
					<th scope="col">Action</th>
					</tr>
				</thead>
				<tbody>
					<? 
					$a =1;
					foreach($getAllDownline as $value){
						$val=$value['gen'];
						$gen=explode(',', $val);
						$keys = array_search($id, $gen);
						
						if($keys === 4){	
					?>
							<tr>
							  <th scope="row"><?=$a;?></th>
							  <td><?=$value->user->username;?></td>
							  <td><?=$value->user->email;?></td>
							  <td><?=$value->phones;?></td>
							  <td>
								  <a href="<?=Url::to(['/sms','contact'=>'2'.$value->phones])?>"><i class="mdi mdi-message-reply"></i></a>
								  |<a href="<?=Url::to(['/email','id'=>$value->user->email])?>" ><i class="mdi mdi-email"></i></a>|
								  <a href="<?=Url::to(['viewgen','id'=>$value->user->id])?>"><i class="mdi mdi-eye"></i></a></td>
							</tr>
							<?$a++;?>
					<? }else{ continue;}} ?>                
				</tbody>           
			</table>            
		</div>            
	</div>            
</div>

<?php 
	
$generationViewJs = <<<JS

	 $('.table').DataTable();
JS;
 
$this->registerJs($generationViewJs);
?>
