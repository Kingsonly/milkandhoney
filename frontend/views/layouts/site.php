<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\SiteAsset;
use common\widgets\Alert;
use yii\helpers\Url;

SiteAsset::register($this);
$getPhoneNumber = Yii::$app->sitecomponent->getContent('phone');
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="pagewrap home-page">
<?php $this->beginBody() ?>
	
<header id="header">
			<!-- Top Header Start -->
		<div id="top_header">
			<div class="container">
				<div class="row">
					<div class="col-md-8 col-sm-5">
						<div class="top_contact">
							<ul>
								<li><i class="fa fa-phone" aria-hidden="true"></i>
									<?
										echo Html::encode('yes');
										foreach($getPhoneNumber as $key => $value){?>
									<?= $value['title']?> <?= $value['value']?>
									<? }?>
									</li>
							</ul>
						</div>
					</div>
					<div class="col-md-4 col-sm-7">
						<div class="top_right">
							<ul>
								<li>
									<div class="lan-drop"> <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Eng <span class="caret"></span></a>
										<ul class="dropdown-menu">
											<li><a href="#">sp</a></li>
											<li><a href="#">ch</a></li>
											<li><a href="#">ud</a></li>
										</ul>
									</div>
								</li>
								<li><a href="<?=Url::to(['signup'])?>" class="toogle_btn" >Register</a></li>
								<li><? if(Yii::$app->user->isGuest){ ?><a href="<?=Url::to(['login'])?>" class="toogle_btn" >Login</a>
								<?}else{?>
									<?= Html::a('Logout', Url::to(['logout']), ['data-method' => 'POST','class' => 'toogle_btn']) ?>
									
									<?= Html::a('| Client area', Url::to(['/dashboard']), ['data-method' => 'POST','class' => 'toogle_btn']) ?>
								<?}?>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- Top Header End --> 

		<!-- Nav Header Start -->
		<div id="nav_header">
			<div class="container">
				<div class="row">
					<div class="col-md-12 col-sm-12">
						<nav class="navbar navbar-default nav_edit"> 
							<!-- Brand and toggle get grouped for better mobile display -->
							<div class="navbar-header">
								<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false"> 
									<span class="sr-only">Toggle navigation</span> 
									<span class="icon-bar"></span> 
									<span class="icon-bar"></span> 
									<span class="icon-bar"></span> 
								</button>
								<a class="navbar-brand" href="#"><img class="nav-logo" src="img/logo1.png" alt=""></a> 
							</div>
							<!-- Collect the nav links, forms, and other content for toggling -->
							<div class="collapse navbar-collapse my_nav" id="bs-example-navbar-collapse-1">
								
								<ul class="nav navbar-nav navbar-right nav_text">
									<li class="active"><a href="<?=Url::to(['index'])?>">Home</a></li>
									<li><a href="<?=Url::to(['service'])?>">Services</a></li>
									<li><a href="<?=Url::to(['faq'])?>">F.A.Q</a></li>
									<li><a href="<?=Url::to(['about'])?>">About Us</a></li>
									<li><a href="<?=Url::to(['contact'])?>">Contact</a></li>
								</ul>
							</div>
							<!-- /.navbar-collapse --> 
						</nav>
					</div>
				</div>
			</div>
		</div>
		<!-- Nav Header End -->
		</header>
		
		<!-- Slider Part Start -->
		
		<!-- Slider Part End --> 
		<?=$content?>
	
		<!-- Footer Section Start -->
		<section id="footer">
			<div class="container">
				<div class="row">
					<div class="col-md-3 col-sm-6">
						<div class="footer_widget"> 
							<div class="footer-logo"><a href="index_1.html"><img class="logo-bottom" src="img/logo2.png" alt=""></a></div>
							<div class="footer_contact">
								<p>Netus ut pede mus vestibulum montes. Mus. Pretium. Mattis habitant netus ligula ridiculus a nam bibendum fusce litora. Ac ullamcorper blandit, viverra pellentesque scelerisque. Phasellus aptent sociosqu nec posuere.</p>
							</div>
							<div class="socail_area">
								<ul>
									<li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
									<li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
									<li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
									<li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
									<li><a href="#"><i class="fa fa-vimeo" aria-hidden="true"></i></a></li>
								</ul>
							</div>
						</div>
					</div>
					<div class="col-md-3 col-sm-6">
						<div class="footer_widget">
							<div class="footer-title">
								<h4>Get In Touch</h4>
							</div>
							<div class="footer_contact">
								<ul>
									<li> <i class="fa fa-map-marker" aria-hidden="true"></i>
										<div class="ftr-list">
											<h6 class="touch-title">Office Address</h6>
											<span>1707 Orlando Central pkwy ste 100 Orlando FL, USA</span>
										</div>
									</li>
									<li> <i class="fa fa-phone" aria-hidden="true"></i>
										<div class="ftr-list">
											<h6 class="touch-title">Call Us 24/7</h6>
											<span>(+241) 542 34251, (+241) 234 88232</span>
										</div>
									</li>
									<li> <i class="fa fa-envelope-o" aria-hidden="true"></i>
										<div class="ftr-list">
											<h6 class="touch-title">Email Address</h6>
											<span>info@webmail.com</span> 
										</div>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="col-md-3 col-sm-6">
						<div class="footer_widget">
							<div class="footer-title">
								<h4>Quick Links</h4>
							</div>
							<div class="footer_contact">
								<ul>
									<li><a href="faq.html">Freequinly Ask Question</a></li>
									<li><a href="about.html">About Our Company</a></li>
									<li><a href="our_service.html">Our Professional Services</a></li>
									<li><a href="terms_and_condition.html">Terms and Conditions</a></li>
									<li><a href="submit_property.html">Submit Your Property</a></li>
									<li><a href="#">Become A Member</a></li>
								</ul>
							</div>
						</div>
					</div>
					<div class="col-md-3 col-sm-6">
						<div class="footer_area">
							<div class="footer-title">
								<h4>Newslatter</h4>
							</div>
							<div class="footer_contact">
								<p>Subscribe to our newsletter and we will inform your about newset projects.</p>
								<div class="news_letter">
									<form action="#" method="post">
										<input type="email" name="email" placeholder="Enter Your Email" class="news_email">
										<button type="submit" name="submit" class="btn btn-default">subscribe</button>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- Footer Section End --> 
		
		<!-- Bottom Footer Start -->
		<div id="bottom_footer">
			<div class="reserve_text"> <span>Copyright &copy; 2018 Milk And Honey Brooks All Right Reserve Designed by Tentaculrat technologies</span> </div>
		</div>
		<!-- Bottom Footer End -->
		
		<!-- Scroll to top -->
		<div class="scroll-to-top">
			<a href="#" class="scroll-btn" data-target=".pagewrap"><i class="fa fa-angle-up" aria-hidden="true"></i></a>
		</div>
	

<?php $this->endBody() ?>
	<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5b281e897f2fd9413d4e70e7/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->
</body>
</html>
<?php $this->endPage() ?>
<?php 
	

?>
