<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>
<? $this->title = 'Sms'; ?>
<?php $this->beginBlock('title') ?>
<?= $this->title;?>
<?php $this->endBlock() ?>
<style>
	
* {
  outline:none !important;
	border:none !important;
	margin:0px;
	padding:0px;
	font-family:Courier, monospace;
}

#paper {
	color:#000;
	font-size:20px;
}
#margin {
	margin-left:12px;
	margin-bottom:20px;
	-webkit-user-select: none;
	-moz-user-select: none;
	-ms-user-select: none;
	-o-user-select: none;
	user-select: none; 
}
	
#text {
	width:100%;
	overflow:hidden;
	background-color:#FFF;
	color:#222;
	font-family:Courier, monospace;
	font-weight:normal;
	font-size:24px;
	resize:none;
	line-height:40px;
	padding-left:100px;
	padding-right:100px;
	padding-top:45px;
	padding-bottom:34px;
	background-image:url(https://static.tumblr.com/maopbtg/E9Bmgtoht/lines.png), url(https://static.tumblr.com/maopbtg/nBUmgtogx/paper.png);
	background-repeat:repeat-y, repeat;
	-webkit-border-radius:12px;
	border-radius:12px !important;
	-webkit-box-shadow: 0px 2px 14px #000;
	box-shadow: 0px 2px 14px #000;
	border-top:1px solid #FFF;
	
}
#title, #sms-contact {
	background-color:transparent;
	border-bottom:3px solid #000 !important;
	border-top:none !important;
	border-right:none !important;
	border-left:none !important;
	
	color:#000;
	font-size:20px;
	font-family:Courier, monospace;
	height:28px;
	font-weight:bold;
	width:220px;
	display: inline !important;
}
#button {
	cursor:pointer;
	margin-top:20px;
	right:0;
	height:40px;
	padding-left:24px;
	padding-right:24px;
	font-family:Arial, Helvetica, sans-serif;
	font-weight:bold;
	font-size:20px;
	color:#FFF;
	text-shadow: 0px -1px 0px #000000;
	-webkit-border-radius:8px;
	border-radius:8px;
	border-top:1px solid #FFF;
	-webkit-box-shadow: 0px 2px 14px #000;
	box-shadow: 0px 2px 14px #000;
	background-color: #62add6;
	background-image:url(https://static.tumblr.com/maopbtg/ZHLmgtok7/button.png);
	background-repeat:repeat-x;
}
#button:active {
	zoom: 1;
	filter: alpha(opacity=80);
	opacity: 0.8;
}
#button:focus {
	zoom: 1;
	filter: alpha(opacity=80);
	opacity: 0.8;
}
	
	
	
.preloader-1 {
  
  width: 66px;
  height: 5px;
  display:none;
}






.preloader-1 .line {
  width: 3px;
  height: 5px;
  background: #fff;
  display: inline-block;
  animation: opacity-1 1000ms infinite ease-in-out;
}

.preloader-2 .line {
  width: 1px;
  height: 12px;
  background: #fff;
  margin: 0 1px;
  display: inline-block;
  animation: opacity-2 1000ms infinite ease-in-out;
}

.preloader-1 .line-1, .preloader-2 .line-1 { animation-delay: 800ms; }
.preloader-1 .line-2, .preloader-2 .line-2 { animation-delay: 600ms; }
.preloader-1 .line-3, .preloader-2 .line-3 { animation-delay: 400ms; }
.preloader-1 .line-4, .preloader-2 .line-4 { animation-delay: 200ms; }
.preloader-1 .line-6, .preloader-2 .line-6 { animation-delay: 200ms; }
.preloader-1 .line-7, .preloader-2 .line-7 { animation-delay: 400ms; }
.preloader-1 .line-8, .preloader-2 .line-8 { animation-delay: 600ms; }
.preloader-1 .line-9, .preloader-2 .line-9 { animation-delay: 800ms; }

@keyframes opacity-1 { 
  0% { 
    opacity: 1;
  }
  50% { 
    opacity: 0;
  }
  100% { 
    opacity: 1;
  }  
}

@keyframes opacity-2 { 
  0% { 
    opacity: 1;
    height: 15px;
  }
  50% { 
    opacity: 0;
    height: 12px;
  }
  100% { 
    opacity: 1;
    height: 15px;
  }  
}

</style>

<div class="row">
	<div class="col-md-12">
		<div class="white-box">
			
			<div id="wrapper" class="row">
				<div class="col-sm-6">
					<?php $form = ActiveForm::begin(['action' => ['sms/sendsms'],'id' => 'paper']); ?>

                <?= $form->field($model, 'sender')->textInput(['autofocus' => true,'id' => 'title']); ?>

                <?= $form->field($model, 'contact')->textInput(['value'=>($contact > 0)?$contact:'','readonly' => ($contact > 0)?true:false ]);  ?>
				<?= $form->field($model, 'body')->textarea(['rows' => 6, 'id' => 'text', 'value' => ($id >0)?'we have a new property you might like please click on the link to view property details . http://localhost/milkhoney/frontend/web/index.php?r=site/propertydetails&id='.$id.'&ref='.yii::$app->user->identity->id:'', 'readonly' => ($id > 0)?true:false ]); ?>

                <?= Html::submitButton('<span id="sendtext">Send</span><span class="preloader-1">
				  <span>Loading</span>
				  <span class="line line-1"></span>
				  <span class="line line-2"></span>
				  <span class="line line-3"></span>
				  <span class="line line-4"></span>
				  <span class="line line-5"></span>
  
  				  </span>', ['class' => 'btn btn-primary', 'name' => 'contact-button','id' => 'button']); ?>

            <?php ActiveForm::end(); ?>
				</div>
				<div class="col-sm-6">
					
					<table class="table table-striped">
					<tr>
						<td>SN</td>
						<td>Numbers</td>
						<td>Details</td>
						<td>Date</td>
					</tr>
					<? foreach($findUserSms as $smsHistory){?>
					<tr>
						<td>SN</td>
						<td><?=$smsHistory->email_contact;?></td>
						<td><?=$smsHistory->email_content;?></td>
						<td><?=$smsHistory->date_sent;?></td>
					</tr>
					<? }?>
					</table>
				</div>
				
			</div>
			 <div>
				 <span id="remaining">
					 160
				 </span>
				 &nbsp;Character
				 <span class="cplural">
					 s
				 </span> 
				 Remaining
			</div>
    		<div>Total&nbsp;<span id="messages">1</span>&nbsp;Message<span class="mplural">s</span>&nbsp;<span id="total">0</span>&nbsp;Character<span class="tplural">s</span></div>
		</div>
	</div>
</div>
	

<?php 
	
$smsform = <<<JS
	part1Count = 160;
    part2Count = 145;
    part3Count = 152;
 
    $('#text').keyup(function(){
        var chars = $(this).val().length;
            messages = 0;
            remaining = 0;
            total = 0;
        if (chars <= part1Count) {
            messages = 1;
            remaining = part1Count - chars;
        } else if (chars <= (part1Count + part2Count)) { 
            messages = 2;
            remaining = part1Count + part2Count - chars;
        } else if (chars > (part1Count + part2Count)) { 
            moreM = Math.ceil((chars - part1Count - part2Count) / part3Count) ;
            remaining = part1Count + part2Count + (moreM * part3Count) - chars;
            messages = 2 + moreM;
        }
        $('#remaining').text(remaining);
        $('#messages').text(messages);
        $('#total').text(chars);
        if (remaining > 1) $('.cplural').show();
            else $('.cplural').hide();
        if (messages > 1) $('.mplural').show();
            else $('.mplural').hide();
        if (chars > 1) $('.tplural').show();
            else $('.tplural').hide();
    });
    $('#text').keyup();





$('#paper').on('beforeSubmit', function (e) {
	$('#sendtext').hide();
	$('.preloader-1').show();
    var \$form = $(this);
	
    $.post(\$form.attr('action'),\$form.serialize())
    .always(function(result){
	
	res = result.split(" ");
   if(res[0] =='OK'){
   	$('.preloader-1').hide();
	   $('#sendtext').show().text('SMS have been sent and you made use of '+res[1]+' unit');
		
	
	
    }else{
    
		$('#sendtext').show().text('somthing went wrong and sms did not deliver');
		$('.preloader-1').hide();
	
    }
    }).fail(function(){
    console.log('Server Error');
    });
	
	setTimeout(function(){ 
	
		$('#sendtext').show().text('Send');
		\$form.reset();

	}, 5000);
    return false;
    
    
});
JS;
 
$this->registerJs($smsform);
?>
