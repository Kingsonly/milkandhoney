<?php
use yii\helpers\Url;
use yii\helpers\Html;
/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<section id="slider">
	<div id="carousel-example-generic" class="carousel slide" data-ride="carousel"> 
				<!-- Indicators -->
				
		<ol class="carousel-indicators my_carousel">
			<? $i = 0; foreach($slider as $key => $value){?>
				<li data-target="#carousel-example-generic" data-slide-to="<?= $i; ?>" class="<? echo ($i == 0)?'active':''; ?>"></li>
			<? $i++; }?>
		</ol>

		<!-- Wrapper for slides -->
		<div class="carousel-inner" role="listbox">
			<? $j = 0; foreach($slider as $key => $value){?>
				<div class="item <? echo ($j == 0)?'active':''; ?>">

				<?= Html::img('@web/images/sliders/'.$value['title'], ['alt' => 'img']) ?>
				<div class="carousel-caption">
					<div class="container">
						<div class="slider_text">

							<p><?= $value['value']; ?></p>

						</div>
					</div>
				</div>
			</div>
			<? $j++; }?>

		</div>

		<!-- Controls --> 
		<a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
			<div class="lef_btn">prev</div>
			<span class="sr-only">Previous</span> </a> <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
			<div class="right_btn">next</div>
		<span class="sr-only">Next</span> </a> 
	</div>
		
</section>


<section id="offer">
			<div class="container">
				<div class="row">
					<?  foreach($features as $key => $value){?>
					<div class="col-md-3 col-sm-6">
						<div class="offer_area wow fadeInLeft" data-wow-delay="200ms" data-wow-duration="1000ms">
							<div class="circle_area"><i class="flaticon-home-1"></i></div>
							<a href="#"><h5 class="offer-title"><?=$value['title']; ?></h5></a>
							<p>
								
								<?=$value['value']; ?>
							</p>
							
						</div>
					</div>
					<? }?>
					
				</div>
			</div>
		</section>
		<!-- Offer Part End --> 
		
		<!-- Recent Property Start -->
		<section id="recent_property">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="title-row">
							<h3 class="section_title_blue">Recent <span>Properties</span></h3>
						</div>
						<a href="property_grid.html" class="property_link">View All Properties</a>
					</div>
				</div>
				<div class="row">
					<? foreach($properties as $property){ ?>
					
					<div class="col-md-4 col-sm-6">
						<div class="property_grid">
							<div class="img_area">
								<div class="sale_btn"><?=$property->type?></div>
								<a href="<?=Url::to(['propertydetails','id' => $property['property_id']]) ?>"><?= Html::img('@web/images/property/'.$property->photo.'', ['alt' => 'My logo']) ?></a>
								<div class="sale_amount"><?=$property->property_date;?></div>
								<div class="hover_property">
									<ul>
										<li><a href="#"><i class="fa fa-search" aria-hidden="true"></i></a></li>
										<li><a href="#"><i class="fa fa-link" aria-hidden="true"></i></a></li>
									</ul>
								</div>
							</div>
							<div class="property-text"> 
								<a href="<?=Url::to(['propertydetails','id' => $property['property_id']]) ?>">
									<h5 class="property-title"><?=$property->property_name?></h5>
								</a> <span><i class="fa fa-map-marker" aria-hidden="true"></i> <?=$property->location?>( <?=$property->state?> )</span>
								<div class="quantity">
									<ul>
										<? foreach($property->propertyAddon as $addons){ ?>
										<li><span><?=$addons->title;?></span><?=$addons->description;?></li>
										<?}?>
									</ul>
								</div>
							</div>
							<div class="bed_area">
								<ul>
									<li><?=$property->price?></li>
									
									
									
								</ul>
								
							</div>
							
							
						</div>
					</div>
					<?}?>
				</div>
			</div>
		</section>
		<!-- Recent Property End --> 
		
		<!-- Service Section Start 
		<section id="service_part">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="title-row">
							<h3 class="section_title_blue"><span>What you are looking for?</span></h3>
							<div class="sub-title">
								<p>Pellentesque porttitor dolor natoque pretium. Scelerisque Quisque, vel curabitur lobortis potenti primis praesent volutpat mi nonummy faucibus tempor consequat vulputate.</p>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-3 col-sm-6">
						<div class="service_area wow fadeInUp" data-wow-delay="100ms" data-wow-duration="1500ms">
							<div class="service_icon"><i class="glyph-icon flaticon-home"></i></div>
							<a href="property_grid.html"><h4 class="service_title">House</h4></a>
							<p>Nisi. Tellus lobortis dapibus erat eu et. Senectus quam vitae in arcu nisi quam</p>
						</div>
					</div>
					<div class="col-md-3 col-sm-6">
						<div class="service_area wow fadeInUp" data-wow-delay="200ms" data-wow-duration="1500ms">
							<div class="service_icon"><i class="glyph-icon flaticon-signs"></i></div>
							<a href="property_grid.html"><h4 class="service_title">Land</h4></a>
							<p>Nisi. Tellus lobortis dapibus erat eu et. Senectus quam vitae in arcu nisi quam</p>
						</div>
					</div>
					<div class="col-md-3 col-sm-6">
						<div class="service_area wow fadeInUp" data-wow-delay="300ms" data-wow-duration="1500ms">
							<div class="service_icon"><i class="glyph-icon flaticon-office"></i></div>
							<a href="property_grid.html"><h4 class="service_title">Office</h4></a>
							<p>Nisi. Tellus lobortis dapibus erat eu et. Senectus quam vitae in arcu nisi quam</p>
						</div>
					</div>
					<div class="col-md-3 col-sm-6">
						<div class="service_area wow fadeInUp" data-wow-delay="400ms" data-wow-duration="1500ms">
							<div class="service_icon"><i class="glyph-icon flaticon-shop"></i></div>
							<a href="property_grid.html"><h4 class="service_title">Business</h4></a>
							<p>Nisi. Tellus lobortis dapibus erat eu et. Senectus quam vitae in arcu nisi quam </p>
						</div>
					</div>
				</div>
			</div>
		</section>
		
		
	
		<section id="featured">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="title-row">
							<h3 class="section_title_blue">Featured <span>Property</span></h3>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="property_slide">
							<div class="item">
								<div class="property_grid">
									<div class="img_area">
										<div class="sale_btn">Rent</div>
										<div class="featured_btn">Featured</div>
										<a href="#"><img src="img/property_grid/property_grid-1.png" alt=""></a>
										<div class="sale_amount">1 Month Ago</div>
										<div class="hover_property">
											<ul>
												<li><a href="#"><i class="fa fa-search" aria-hidden="true"></i></a></li>
												<li><a href="#"><i class="fa fa-link" aria-hidden="true"></i></a></li>
											</ul>
										</div>
									</div>
									<div class="property-text"> 
										<a href="#">
											<h5 class="property-title">Park Road Appartment Rent</h5>
										</a><span><i class="fa fa-map-marker" aria-hidden="true"></i> 3225 George Street Brooksville, FL 34610</span>
										<div class="quantity">
											<ul>
												<li><span>Area</span>2100 Sqft</li>
												<li><span>Rooms</span>8</li>
												<li><span>Beds</span>4</li>
												<li><span>Baths</span>3</li>
												<li><span>Garage</span>1</li>
											</ul>
										</div>
									</div>
									<div class="bed_area">
										<ul>
											<li>$1600/mo</li>
											<li class="flat-icon"><a href="#"><i class="flaticon-like"></i></a></li>
											<li class="flat-icon"><a href="#"><i class="flaticon-connections"></i></a></li>
										</ul>
									</div>
								</div>
							</div>
							<div class="item">
								<div class="property_grid">
									<div class="img_area">
										<div class="sale_btn">Sale</div>
										<div class="featured_btn">Featured</div>
										<a href="#"><img src="img/property_grid/property_grid-2.png" alt=""></a>
										<div class="sale_amount">5 Days Ago</div>
										<div class="hover_property">
											<ul>
												<li><a href="#"><i class="fa fa-search" aria-hidden="true"></i></a></li>
												<li><a href="#"><i class="fa fa-link" aria-hidden="true"></i></a></li>
											</ul>
										</div>
									</div>
									<div class="property-text"> 
										<a href="#">
											<h5 class="property-title">Park Road Appartment Rent</h5>
										</a><span><i class="fa fa-map-marker" aria-hidden="true"></i> 3494 Lyon Avenue Middleboro, MA 02346 </span>
										<div class="quantity">
											<ul>
												<li><span>Area</span>1100 Sqft</li>
												<li><span>Rooms</span>5</li>
												<li><span>Beds</span>2</li>
												<li><span>Baths</span>2</li>
												<li><span>Garage</span>1</li>
											</ul>
										</div>
									</div>
									<div class="bed_area">
										<ul>
											<li>$1,410,000</li>
											<li class="flat-icon"><a href="#"><i class="flaticon-like"></i></a></li>
											<li class="flat-icon"><a href="#"><i class="flaticon-connections"></i></a></li>
										</ul>
									</div>
								</div>
							</div>
							<div class="item">
								<div class="property_grid">
									<div class="img_area">
										<div class="sale_btn">Rent</div>
										<div class="featured_btn">Featured</div>
										<a href="#"><img src="img/property_grid/property_grid-3.png" alt=""></a>
										<div class="sale_amount">10 Days Ago</div>
										<div class="hover_property">
											<ul>
												<li><a href="#"><i class="fa fa-search" aria-hidden="true"></i></a></li>
												<li><a href="#"><i class="fa fa-link" aria-hidden="true"></i></a></li>
											</ul>
										</div>
									</div>
									<div class="property-text"> 
										<a href="#">
											<h5 class="property-title">Park Road Appartment Rent</h5>
										</a><span><i class="fa fa-map-marker" aria-hidden="true"></i> 39 Parrill Court Oak Brook, IN 60523 </span>
										<div class="quantity">
											<ul>
												<li><span>Area</span>1600 Sqft</li>
												<li><span>Rooms</span>7</li>
												<li><span>Beds</span>4</li>
												<li><span>Baths</span>3</li>
												<li><span>Garage</span>1</li>
											</ul>
										</div>
									</div>
									<div class="bed_area">
										<ul>
											<li>$1200/mo</li>
											<li class="flat-icon"><a href="#"><i class="flaticon-like"></i></a></li>
											<li class="flat-icon"><a href="#"><i class="flaticon-connections"></i></a></li>
										</ul>
									</div>
								</div>
							</div>
							<div class="item">
								<div class="property_grid">
									<div class="img_area">
										<div class="sale_btn">Rent</div>
										<div class="featured_btn">Featured</div>
										<a href="#"><img src="img/property_grid/property_grid-4.png" alt=""></a>
										<div class="sale_amount">3 Days Ago</div>
										<div class="hover_property">
											<ul>
												<li><a href="#"><i class="fa fa-search" aria-hidden="true"></i></a></li>
												<li><a href="#"><i class="fa fa-link" aria-hidden="true"></i></a></li>
											</ul>
										</div>
									</div>
									<div class="property-text"> 
										<a href="#">
											<h5 class="property-title">Central Road Appartment Rent</h5>
										</a><span><i class="fa fa-map-marker" aria-hidden="true"></i> 39 Parrill Court Oak Brook, IN 60523 </span>
										<div class="quantity">
											<ul>
												<li><span>Area</span>1800 Sqft</li>
												<li><span>Rooms</span>8</li>
												<li><span>Beds</span>4</li>
												<li><span>Baths</span>3</li>
												<li><span>Garage</span>1</li>
											</ul>
										</div>
									</div>
									<div class="bed_area">
										<ul>
											<li>$1200/mo</li>
											<li class="flat-icon"><a href="#"><i class="flaticon-like"></i></a></li>
											<li class="flat-icon"><a href="#"><i class="flaticon-connections"></i></a></li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		Featured Section End --> 
		
		<!-- Register Section Start -->
		<section id="register-banner">
			<div class="container">
				<div class="row">
					<div class="col-md-9">
						<div class="reg_banner">
							<h4 class="reg_banner_title">Are you looking for a House or Customer for your Property sale?</h4>
							<span>Please click the button for register, we will become your best agent and help you for both.</span>
						</div>
					</div>
					<div class="col-md-3">
						<div class="register_btn">
							<a href="<?=Url::to(['site/signup']);?>" class="btn btn-primary">Register Now</a>
						</div>
					</div>
				</div>
			</div>
		</section>
<?
$invoiceform = <<<JS
        
   (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

		  ga('create', 'UA-106986305-1', 'auto');
		  ga('send', 'pageview'); 
});
JS;
 
$this->registerJs($invoiceform);
?>
		<!-- Register Section End --> 
		
