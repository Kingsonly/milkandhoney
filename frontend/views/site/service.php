<?
use yii\helpers\Url;
?>
<style>
	#banner{
		background-image: url(<?='../web/uniland/'.$banner->value;?>) !important;
	}
</style>
<section id="banner">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="banner_area">
					<h3 class="page_title"><?= $banner->title; ?></h3>
					
				</div>
			</div>
		</div>
	</div>
</section>

<!-- Service Section Start -->
<section id="service">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="row">
					<div class="col-md-12">
						<div class="service_detail">
							<ul>
								<? foreach($content as  $key => $value){ ?>
								<li>
									<div class="service_circle"><i class="<?= $value->icon; ?>"></i></div>
									<div class="service_text">
										<h4 class="service_title"><?= $value->title; ?></h4>
										<p>
											<?= $value->value; ?>
										</p>
									</div>
								</li>
								<?} ?>
								
							</ul>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="service_banner">
							<div class="banner_text">
								<h5 class="banner_title_green"><?= $contact->title; ?></h5>
								<p>
									<?= $contact->value; ?>
								</p>
								<a class="btn btn-primary" href="<?= Url::to(['contact'])?>">contact us</a>
							</div>
						</div>
					</div>
				</div>
				
			</div>
					
		</div>
		
	</div>
		
</section>
		<!-- Service Section End -->
		
		<!-- Register Section Start -->
<section id="register-banner">
	<div class="container">
		<div class="row">
			<div class="col-md-9">
				<div class="reg_banner">
					<h4 class="reg_banner_title">Are you looking for a House or Customer for your Property sale?</h4>
					<span>Please click the button for register, we will become your best agent and help you for both.</span>
				</div>
			</div>
			<div class="col-md-3">
				<div class="register_btn">
					<a href="#" class="btn btn-primary">Register Now</a>
				</div>
			</div>
		</div>
	</div>	
</section>