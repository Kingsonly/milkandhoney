<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'About';
$this->params['breadcrumbs'][] = $this->title;
?>

<style>
	#banner{
		background-image: url(<?='../web/uniland/'.$banner->value;?>) !important;
	}
</style>
<section id="banner">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="banner_area">
							<h3 class="page_title">About Us</h3>
							
						</div>
					</div>
				</div>
			</div>
		</section>


<section id="offer">
	<div class="container">
				<div class="row">
					<? foreach($history as $key => $value){?>
					<div class="col-md-3 col-sm-6">
						<div class="offer_area wow fadeInLeft" data-wow-delay="200ms" data-wow-duration="1000ms">
							<div class="circle_area"><i class="<?= $value->icon; ?>"></i></div>
							<a href="#"><h5 class="offer-title"><?= $value->title; ?></h5></a>
							<p>
								<?= $value->value; ?>
							</p>
							
						</div>
					</div>
					<? }?>
			
				</div>
			</div>
</section>
		<!-- Offer Part End -->  
		
		<!-- Who We Are Part Start -->
		<section id="who_are">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-7">
						<div class="row">
							<div class="who_text">
								<div class="section_title_white"><h2><?= $intro->title; ?></h2></div>
								<?= $intro->value; ?>
								
							</div>
						</div>
					</div>
					
				</div>
			</div>
		</section>
		<!-- Who We Are Part End -->
		
		<!-- About Accordion Part Start -->
		<section id="about_description">
			<div class="container">
				<div class="row">
					<? foreach($vissionmission as $key => $value){?>
					<div class="col-md-6">
						<div class="we_do">
							<div class="title-row"><h2 class="section_title_blue"> <span> <?= $value->title; ?></span></h2></div>
							<p>
								<?= $value->value; ?>
							</p>
							
						</div>
					</div>
					<? }?>
				</div>
			</div>
		</section>
		<!-- About Accordion Part End -->

<section id="register-banner">
			<div class="container">
				<div class="row">
					<div class="col-md-9">
						<div class="reg_banner">
							<h4 class="reg_banner_title">Are you looking for a House or Customer for your Property sale?</h4>
							<span>Please click the button for register, we will become your best agent and help you for both.</span>
						</div>
					</div>
					<div class="col-md-3">
						<div class="register_btn">
							<a href="#" class="btn btn-primary">Register Now</a>
						</div>
					</div>
				</div>
			</div>
		</section>
