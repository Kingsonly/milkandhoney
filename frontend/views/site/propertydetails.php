<section id="single_property">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="property_slider">
					<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">

						<!-- Wrapper for slides -->
						<div class="carousel-inner" role="listbox">
							<div class="item active">
								<img src="img/single_property/1.png" alt="">
							</div>
							<div class="item">
								<img src="img/single_property/2.png" alt="">
							</div>
							<div class="item">
								<img src="img/single_property/3.png" alt="">
							</div>
						</div>

						<!-- Controls -->
						<a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
							<div class="lef_btn">prev</div>
							<span class="sr-only">Previous</span>
						</a>
						<a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
							<div class="right_btn">next</div>
							<span class="sr-only">Next</span>
						</a>
					</div>
				</div>
			</div>
		</div>	
		<div class="row">
			<div class="col-md-12">					
				<div class="row">
					<div class="col-md-12">
						<div class="detail_text">
							<div class="property-text">
								<h4 class="property-title"><?= $property->property_name; ?></h4>
								<span><i class="fa fa-map-marker" aria-hidden="true"></i> <?= $property->location; ?> |<?= $property->state; ?></span>
								
								<blockquote>
									<?= $property->description; ?>
								</blockquote>
								
							</div>
						</div>
						<div class="more_information">
							<h4 class="inner-title">More Information</h4>
							<div class="profile_data">
								<ul>
									<? foreach($property->propertyAddon as $key => $value){ ?>
									<li>
										<span><?= $value->title; ?> :</span> 
										<?= $value->description; ?>
									</li>
									<? }?>
									
								</ul>
							</div>
						</div>
						
					</div>
				</div>


			</div>
		</div>

	</div>
</section>
		<!-- Single Property End --> 
		
		