<style>
	#banner{
		background-image: url(<?='../web/uniland/'.$banner->value;?>) !important;
	}
</style>
<section id="banner">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="banner_area">
							<h3 class="page_title">F.A.Q</h3>
							
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- Banner Section End -->
		
		<!-- FAQ Section Start -->
		<section id="faq_area">
			<div class="container">
				<div class="row">
					
					<div class="col-md-12 col-sm-12">
						<div class="info-pages">
							<div class="row">
								<div class="col-md-12 col-sm-12">
									<h3 class="inner-title"><?= $intro->title; ?></h3>
									<p>
										<?= $intro->value; ?>
									</p>
								</div>
							</div>
							
							<div class="row">
								<div class="col-md-12 col-sm-12">
									<div class="accordion">
										<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
											<? $i=1; foreach($content as $key => $value){ ?>
											<div class="panel panel-default accordion_panel">
												<div class="panel-heading accordian_title" role="tab" id="heading<?= $value->id;?>">
													<div class="panel-title">
														<a class="<?=($i===1)?'':'collapsed';?>" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse<?= $value->id;?>" aria-expanded="true" aria-controls="collapse<?= $value->id;?>">
															
															<?= $value->title;?> 
															<span><i class="fa fa-angle-right" aria-hidden="true"></i></span>
														</a>
													</div>
												</div>
												<div id="collapse<?= $value->id;?>" class="panel-collapse collapse <?=($i===1)?'in':'';?>" role="tabpanel" aria-labelledby="heading<?= $value->id;?>">
													<div class="panel-body accordian_para">
														<p>
															<?= $value->value;?>
															</p>
													</div>
												</div>
											</div>
											<? $i++; } ?>
											
										</div>
									</div>
								</div>
							</div>
							
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- FAQ Section End -->
		