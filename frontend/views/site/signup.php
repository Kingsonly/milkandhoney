<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Signup';
$this->params['breadcrumbs'][] = $this->title;
?>

<section id="banner">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="banner_area">
							<h3 class="page_title">Registration</h3>
							<div class="page_location">
								<a href="index_1.html">Home</a> 
								<i class="fa fa-angle-right" aria-hidden="true"></i>
								<span>Registration</span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>


<section class="reg_popup">
			<!-- Modal -->
			<div id="myModal">
				<div class="modal-dialog toggle_area" role="document">
					<div class="modal-header toggle_header">
						<h4 class="inner-title" style="display:inline-block">Create An Account | </h4>
						<span style="display:inline-block"><em><small style="color:red;">You where invited by (<?=$refUsername;?>)</small></em></span>
					</div>
					<div class="modal-body">
						<?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>
							<div class="signup_option">
								<div class="form-group">
									<?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>
								</div>
								<div class="form-group">
								<?= $form->field($model, 'email') ?>
								</div>
								<div class="form-group">
								<?= $form->field($model, 'password')->passwordInput() ?>
								</div>
								<div class="form-group">
								<?= $form->field($consultantModel, 'bank')->textInput() ?>
								</div>
								<div class="form-group">
								<?= $form->field($consultantModel, 'acc_num')->textInput() ?>
								</div>
								<div class="form-group">
								<?= $form->field($consultantModel, 'phones')->textInput() ?>
								</div>
								<div class="submit_area">
									<?= Html::submitButton('Signup', ['class' => 'btn btn-default ', 'name' => 'signup-button']) ?>
								</div>
							</div>
            			<?php ActiveForm::end(); ?>
						
					</div>
					<div class="modal-footer"> <span>Click and read our terms and condition<a href="#">Terms and Condition</a></span> </div>
				</div>
			</div>	
		</section>
