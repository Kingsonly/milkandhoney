<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>

<section id="banner">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="banner_area">
							<h3 class="page_title">Sign In</h3>
							<div class="page_location">
								<a href="index_1.html">Home</a> 
								<i class="fa fa-angle-right" aria-hidden="true"></i>
								<span>Sign In</span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

<section class="login-box">
			<!-- Modal -->
			<div id="myModal_two">
				<div class="modal-dialog toggle_area" role="document">
					<div class="modal-header toggle_header">
						<h4 class="inner-title">Sign In Account</h4>
					</div>
					<div class="modal-body login_body">
						<p>Welcome to Milk and Honey, Please fill out the following fields to login!</p>
						<div class="login_option">
							<?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
								<div class="form-group">
									<?= $form->field($model, 'username')->textInput() ?>
								</div>
								<div class="form-group">
									<?= $form->field($model, 'password')->passwordInput() ?>
								</div>
								<div class="form-group">
								<?= $form->field($model, 'rememberMe')->checkbox() ?>
									 <?= Html::submitButton('Login', ['class' => 'btn btn-default', 'name' => 'login-button']) ?>
								</div>
							 <?php ActiveForm::end(); ?>
						</div>
					<div class="submit_area"><span>Lost your password?  <?= Html::a('Reset it', ['site/request-password-reset']) ?></span></div>
					</div>
					<div class="modal-footer"> <span>Click and read our terms and condition<a href="#">Terms and Condition</a></span> </div>
				</div>
			</div>
		</section>
