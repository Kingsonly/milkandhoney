<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\PasswordResetRequestForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Request password reset';
$this->params['breadcrumbs'][] = $this->title;
?>


<section id="banner">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="banner_area">
							<h3 class="page_title">Sign In</h3>
							<div class="page_location">
								<a href="index_1.html">Home</a> 
								<i class="fa fa-angle-right" aria-hidden="true"></i>
								<span>Sign In</span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

<section class="login-box">
			<!-- Modal -->
			<div id="myModal_two">
				<div class="modal-dialog toggle_area" role="document">
					<div class="modal-header toggle_header">
						<h4 class="inner-title">Request Password Reset  Token</h4>
					</div>
					<div class="modal-body login_body">
						Please fill out your email. A link to reset password will be sent there.
						<div class="login_option">
							<?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
								
								<div class="form-group">
									<?= $form->field($model, 'email')->textInput() ?>
								</div>
								<div class="form-group">
								
									 <?= Html::submitButton('Login', ['class' => 'btn btn-default', 'name' => 'login-button']) ?>
								</div>
							 <?php ActiveForm::end(); ?>
						</div>
				
				</div>
			</div>
		</section>
