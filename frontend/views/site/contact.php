<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = 'Contact';
$this->params['breadcrumbs'][] = $this->title;
?>

 
	
<section id="banner">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="banner_area">
							<h3 class="page_title"><h1><?= Html::encode($this->title) ?></h1></h3>
							<div class="page_location">
								<a href="index_1.html"></a> 
								<i class="fa fa-angle-right" aria-hidden="true"></i>
								<span></span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	
	<section id="contact">
			<div class="container">
				<h2 class="section_title_blue">Get <span>In Touch</span></h2>
				<div class="row">
					<div class="col-md-6">
						<div class="contact_area">
							<p>
								If you have business inquiries or other questions, please fill out the following form to contact us. Thank you.
							</p>

								<div class="row">
									<?php $form = ActiveForm::begin(['id' => 'contact-form']); ?>
										<div class="form-group col-md-6 col-sm-6">
                <?= $form->field($model, 'firstname')->textInput(['id'=> 'firstname']) ?>
									</div>
									
									<div class="form-group col-md-6 col-sm-6">
                <?= $form->field($model, 'lastname')->textInput(['id'=> 'lastname']) ?>
									</div>
									<div class="form-group col-md-6 col-sm-6">
                <?= $form->field($model, 'email')->textInput(['id'=> 'email']) ?>
									</div>
									<div class="form-group col-md-6 col-sm-6">
                <?= $form->field($model, 'subject')->textInput(['id'=> 'subject']) ?>
									</div>
                

                
<div class="form-group col-md-12 col-sm-12">
                <?= $form->field($model, 'body')->textarea(['rows' => 6, 'id' => 'message']) ?>
									</div>
					<div class="form-group col-md-12 col-sm-12">
                <?= $form->field($model, 'verifyCode')->widget(Captcha::className(), [
                    'template' => '<div class="row"><div class="col-lg-3">{image}</div><div class="col-lg-6">{input}</div></div>',
                ]) ?>
									</div>

              <div class="form-group col-md-12 col-sm-12">
                    <?= Html::submitButton('Submit', ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
                </div>

            <?php ActiveForm::end(); ?>
									
									
									
									
									<div class="col-md-12">
										<div class="error-handel">
											<div id="success">Your email sent Successfully, Thank you.</div>
											<div id="error"> Error occurred while sending email. Please try again later.</div>
										</div>
									</div>
							
						</div>
					</div>
					</div>
					<div class="col-md-6">
						<div class="row">
							<div class="col-md-6">
								<div class="contact_right">
									<h5 class="inner-title">Our Address</h5>
									<p>1707 Orlando Central pkwy ste 100 Orlando FL, USA</p>
								</div>
							</div>
							<div class="col-md-6">
								<div class="contact_right">
									<h5 class="inner-title">Contact Info</h5>
									<p>(+241) 542 34251, (+241) 234 88232 info@webmail.com</p>
								</div>
							</div>
						</div>
						<div class="contact_map">
							<div id="map" class="map-canvas"> </div>
						</div>
					</div>
				
			</div>
		</section>
		<!-- Contact Section End -->
		
		<!-- Register Section Start -->
		<section id="register-banner">
			<div class="container">
				<div class="row">
					<div class="col-md-9">
						<div class="reg_banner">
							<h4 class="reg_banner_title">Are you looking for a House or Customer for your Property sale?</h4>
							<span>Please click the button for register, we will become your best agent and help you for both.</span>
						</div>
					</div>
					<div class="col-md-3">
						<div class="register_btn">
							<a href="#" class="btn btn-primary">Register Now</a>
						</div>
					</div>
				</div>
			</div>
		</section>
	
    


