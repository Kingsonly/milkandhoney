<?php
namespace frontend\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use frontend\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use frontend\models\Consultant;
use frontend\models\Userdb;
use frontend\models\PropertyViewCount;
use frontend\models\Properties;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout','signup'],
                'rules' => [
                    
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
					[
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
		$this->layout = 'site';
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex($id = 0)
    {
		
		$getSliderImage = Yii::$app->sitecomponent->getContent('slider');
		$getFeatures = Yii::$app->sitecomponent->getContent('features');
		$getProperties = Yii::$app->sitecomponent->getProperties();
		if($id >= 1 or is_string($id)){
			if(empty($id)){
				return $this->render('index',[
				
				'slider' => $getSliderImage,
				'features' => $getFeatures,
				'properties' => $getProperties,
				
			]);
			}else{
				return $this->redirect(['site/signup','id'=>$id]);
			}
			
		}else{
			return $this->render('index',[
				
				'slider' => $getSliderImage,
				'features' => $getFeatures,
				'properties' => $getProperties,
				
			]);
		}
        
    }
	
	public function actionPropertydetails($id = 0, $reff = 0)
    {
		$getProperty = new Properties();
		$getSingleProperty = $getProperty->find()->where(['property_id' => $id])->one();
		if($reff > 0){
			$propertyViewCount = PropertyViewCount::find()->where(['user_id' => $reff,'property_id' => $id]);
			if(!empty($propertyViewCount)){
				$propertyViewCount->views = $propertyViewCount->views + 1;
				$propertyViewCount->save(false);
				
			} else{
				$insertPropertyViewCount = new PropertyViewCount();
				$insertPropertyViewCount->user_id = $reff;
				$insertPropertyViewCount->property_id = $id;
				$insertPropertyViewCount->views = 1;
				$insertPropertyViewCount->save(false);
				
			}
			
		}
		return $this->render('propertydetails',[
			'property' => $getSingleProperty,
		]);
        
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
		$this->layout = 'site';
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->redirect(['dashboard/']);
        } else {
            $model->password = '';

            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
		$this->layout = 'site';
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact()
    {
		$this->layout = 'site';
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending your message.');
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionAbout()
    {
		$this->layout = 'site';
		$getBanner = Yii::$app->sitecomponent->getSingleServiceContentAboutus('banner');
		$getHistory = Yii::$app->sitecomponent->getServiceContentAboutus('history');
		$getIntroText = Yii::$app->sitecomponent->getSingleServiceContentAboutus('intro');
		$getContent = Yii::$app->sitecomponent->getServiceContentAboutus('vissionmission');
        return $this->render('about',[
			'banner' => $getBanner,
			'history' => $getHistory,
			'intro' => $getIntroText,
			'vissionmission' => $getContent,
		]);
    }
	
	public function actionService()
	{
		$this->layout = 'site';
		$getContent = Yii::$app->sitecomponent->getServiceContent('content');
		$getBanner = Yii::$app->sitecomponent->getSingleServiceContent('banner');
		$getContact= Yii::$app->sitecomponent->getSingleServiceContent('contact');
        return $this->render('service',[
			'content' => $getContent,
			'banner' => $getBanner,
			'contact' => $getContact,
		]);
    }
	
	public function actionFaq()
    {
		$this->layout = 'site';
		$getBanner = Yii::$app->sitecomponent->getSingleServiceContentFaq('banner');
		$getIntroText = Yii::$app->sitecomponent->getSingleServiceContentFaq('intro');
		$getContent = Yii::$app->sitecomponent->getServiceContentFaq('content');
        return $this->render('faq',[
			'banner' => $getBanner,
			'intro' =>  $getIntroText,
			'content' =>  $getContent,
		]);
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup($id=0)
    {
		$this->layout = 'site';
        $model = new SignupForm();
        $consultantModel = new Consultant();
        $userModel = new Userdb();
		$refDetails = [];
		
		if($id === 0){
			//admin details
			$getUsername = $userModel->find()->where(['username' => 'admin'])->one();
			$getConsultant = $consultantModel->find()->where(['user_id' => $getUsername->id])->one();
			
			$refDetails['gen'] = $getConsultant->gen;
			$refDetails['username'] = 'Admin';
			$refDetails['father'] = $getUsername->id;
		}else{
			// get user name from users table and get generation from consultant table
			$getUsernames = $userModel->find()->where(['username' => 'admin'])->one();
			$getConsultant = empty($consultantModel->find()->where(['ref_code'=>$id])->one())?$consultantModel->find()->where(['user_id' => $getUsernames->id])->one():$consultantModel->find()->where(['ref_code'=>$id])->one();
			$getUsername = $userModel->find()->where(['id' => $getConsultant->user_id])->one();
			$refDetails['gen'] = $getConsultant->gen;
			$refDetails['username'] = $getUsername->username;
			$refDetails['father'] = $getUsername->id;
		}
		
        if ($model->load(Yii::$app->request->post()) && $consultantModel->load(Yii::$app->request->post())) {
			
            if ($user = $model->signup()) {
				$userUserName = $user->username;
				$consultantModel->ref_code = $user->username;
				$consultantModel->gen = empty($refDetails['gen'])?$refDetails['father']:$refDetails['father'].','.$refDetails['gen'];
				$consultantModel->father = $refDetails['father'];
				$consultantModel->user_id = $user->id;
				$consultantModel->save(false);
				Yii::$app->mailqueue->compose(['html' => 'consultantsendmail', 'text' => 'consultantsendmailtext'],
					[
						
						'body'  => 'thanks for joining',

					])
				 ->setFrom('from@domain.com')
				 ->setTo($user->email)
				 ->setSubject('Welcome to Milk&Honey')
				 ->setTextBody('thanks for joining')
				 ->send();
				$explodeGen = explode(',',$consultantModel->gen);
				foreach($explodeGen as $value){
					$getUplineEmail = $userModel->find()->where(['id' => $value])->one();
					$uplineEmail = $getUplineEmail->email;
					Yii::$app->mailqueue->compose(['html' => 'consultantsendmail', 'text' => 'consultantsendmailtext'],
					[
						
						'body'  => $userUserName.'is now your downline',

					])
				 ->setFrom('from@domain.com')
				 ->setTo($uplineEmail)
				 ->setSubject('You have a new Downline')
				 ->setTextBody($userUserName.'is now your downline')
				 ->send();
					
				}
					
                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            }
        }

        return $this->render('signup', [
            'model' => $model,
            'consultantModel' => $consultantModel,
			'refUsername' => $refDetails['username'],
        ]);
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
		$this->layout = 'site';
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for the provided email address.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
		$this->layout = 'site';
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }
}
