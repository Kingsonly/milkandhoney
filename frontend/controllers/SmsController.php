<?php
namespace frontend\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use frontend\models\Consultant;
use frontend\models\Sms;
use frontend\models\Message;
use frontend\models\UsersContact;


/**
 * Site controller
 */
class SmsController extends Controller
{
    /**
     * {@inheritdoc}
     */
	
	public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                   
                    [
                        
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
			'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
            
        ];
    }
	

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
		
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex($id = 0,$contact = 0)
    {   
		
		$model = new Sms();
		$messageModel = new Message();
		$getId = $id;
		$findUserSms = $messageModel->find()->where(['sender_user_id' => yii::$app->user->identity->id])->all();
		$convertToArray = str_split($contact);
		array_shift($convertToArray);
		$newContact = implode('',$convertToArray);
		//$newContact2 = $newContact;
        return $this->render('index',[
			'model' => $model,
			'id' => $getId,
			'contact' => $newContact,
			'findUserSms' => $findUserSms,
		]);
    }

    
	function smssend($owneremail,$subacct,$subacctpwd,$sendto,$sender,$message){
        
		$url =
		"http://www.bulksmsbase.com/components/com_spc/smsapi.php?username=".UrlEncode('kingsonly') . "&password=".UrlEncode('P@55w)rd')."&sender=".UrlEncode($sender)."&recipient=".UrlEncode($sendto)."&message=".UrlEncode($message)."&msgid=4";
		
		


		if ($f = @fopen($url, "r")){ 
			$answer = fgets($f, 255);
			if (substr($answer, 0, 1) == "+"){
				return "SMS to $dnr was successful.";
			} elseif($answer){
				return $answer;
			} else {
				return 1;
			}
		} else {
			return 0;
		}


        
       
    }
	
	
	
	public function actionSendsms()
    {
		$model = new Sms();
		$messageModel = new Message();
		if($model->load(Yii::$app->request->post())){
			
			$abc = Yii::$app->request->post();
			$sender = $abc['Sms']['sender'];
			$contact = $abc['Sms']['contact'];
			$body = $abc['Sms']['body'];
			
			$this->smssend('kingsonly13c@gmail.com','KINGSONLY','firstoctober',$contact,$sender,$body);
			
			// save into message table 
			$messageModel -> subject = $sender;
			$messageModel -> email_contact = $contact;
			$messageModel -> email_content = $body;
			$messageModel ->  sender_user_id  = yii::$app->user->identity->id;
			$messageModel-> save(false);
			
			// save contact if its not already in the db
			
			if(strpos($contact, ',') !== false){
				// check if user exists 
				
				foreach(explode(',',$contact) as $number){
					$newContactModel = new UsersContact();
					$exists = $newContactModel -> find()->where( [ 'phone_number' => $number ] )->exists();
					if(!$exists){
						$newContactModel -> phone_number = $number;
						$newContactModel -> user_id = yii::$app->user->identity->id;
						$newContactModel -> contact_type = 'phonenumber';
						$newContactModel ->save(false);
					}
				}
				
			} else{
				$newContactModel = new UsersContact();
				$exists = $newContactModel -> find()->where( [ 'phone_number' => $contact ] )->exists();
					if(!$exists){
						$newContactModel -> phone_number = $contact;
						$newContactModel -> user_id = yii::$app->user->identity->id;
						$newContactModel -> contact_type = 'phonenumber';
						$newContactModel ->save(false);
					}
				
			}
				
				
		}
        
    }

    
}
