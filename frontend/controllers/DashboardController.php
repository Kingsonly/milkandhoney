<?php
namespace frontend\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use frontend\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use frontend\models\Consultant;
use frontend\models\MailQueue;
use frontend\models\Message;
use frontend\models\Properties;
use frontend\models\Commissions;
use frontend\models\Clients;
use frontend\models\UsersContact;



/**
 * Site controller
 */
class DashboardController extends Controller
{
    /**
     * {@inheritdoc}
     */
	
	public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                   
                    [
                        
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
			'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
            
        ];
    }
	

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
		
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
		$this->layout = 'dashboard';
		// All model instance
		$consultantModel = new Consultant();
		$mailModel = new MailQueue();
		$messageModel = new Message();
		$propertiesModel = new Properties();
		$commissionsModel = new Commissions();
		$clientsModel = new Clients();
		
		// Model instance implimentations
		
		$allDownlines = $consultantModel -> find()->where(['in','gen', yii::$app->user->identity->id])  -> count();
		
		$allSentEmail = $mailModel -> find()->where(['user_id' => yii::$app->user->identity->id]) -> andWhere(['>','attempts',0])  -> count();
		
		$allClients = $clientsModel -> find()->where(['consultant_id' => yii::$app->user->identity->id]) -> count();
		
		$allSentSms = $messageModel -> find()->where(['sender_user_id' => yii::$app->user->identity->id])   -> count();
		
		$allProperties = $propertiesModel -> find()->where(['status' => 0])   -> count();
		
		$allCommissions = $commissionsModel -> find()->where(['consultant_id' => yii::$app->user->identity->id])   -> count();
		
		return $this->render('index',[
			'totalGenerations' => $allDownlines,
			'totalEmails' => $allSentEmail,
			'totalSms' => $allSentSms,
			'totalProperties' => $allProperties,
			'totalCommissions' => $allCommissions,
			'totalClients' => $allClients,
		]);
        
    }
	
	/**
     * Displays all contacts of an individaul.
     *
     * @return mixed
     */
	public function actionUsersContact()
    {
		// All model instance
		$usersContacts = new UsersContact();
		
		$contactDetails = $usersContacts->find()->where(['user_id' => yii::$app->user->identity->id])->all();
	
		return $this->render('contacts',[
			'contactDetails' => $contactDetails,
			
		]);
        
    }
	
	public function actionUsersContactUpdate($id)
    {
		// All model instance
		$usersContacts = new UsersContact();
		$model = $usersContacts->findOne($id);
		if($model->load(Yii::$app->request->post())  && $model->save(false)){
			return 1;
		} else{
			return 0;
		}
		
    }
	
	public function actionUsersProfileUpdate()
    {
		// All model instance
		$usersProfile = new Consultant();
		$model = $usersProfile->findOne(['user_id' => yii::$app->user->identity->id]);
		if($model->load(Yii::$app->request->post())  && $model->save(false)){
			return 1;
		} else{
			return 0;
		}
		
    }
	
	public function actionUsersContactDelete($id)
    {
		// All model instance
		$usersContacts = new UsersContact();
		$model = $usersContacts->findOne($id);
		if($model->delete()){
			return 1;
		} else{
			return 0;
		}
		
    }
	
	public function actionProfile(){
		$model = new Consultant();
		$getConsultantDetails = $model->find()->select(['f_name','l_name','dob','phones','country','state','city','address','acc_name','bank','acc_num'])->where(['user_id' => yii::$app->user->identity->id])->asArray()->one();
		return $this->render('profile',[
			'profile' => $getConsultantDetails,
			'model' => $model,
			
		]);
		
	}

    
}
