<?php
namespace frontend\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use frontend\models\Consultant;
use frontend\models\EmailForm;
use frontend\models\Userdb;
use frontend\models\UsersContact;
use frontend\models\MailQueue;


/**
 * Site controller
 */
class EmailController extends Controller
{
    /**
     * {@inheritdoc}
     */
	
	public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                   
                    [
                        
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
			'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
            
        ];
    }
	

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
		
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex($id = 0,$contact = 0)
    {   
		// model instance
		$model = new EmailForm();
		$mailQueueModel = new MailQueue();
		
		$getId = $id;
		$convertToArray = str_split($contact);
		$getUsersEmail = $mailQueueModel->find()->where(['user_id' => yii::$app->user->identity->id]) ->all();
		array_shift($convertToArray);
		$newContact = implode('',$convertToArray);
		//$newContact2 = $newContact;
        return $this->render('index',[
			'model' => $model,
			'id' => $getId,
			'contact' => $newContact,
			'emails' => $getUsersEmail,
		]);
    }

    
	
	
	
	
	public function actionSendemail()
    {
		$model = new EmailForm();
		if($model->load(Yii::$app->request->post())){
			
			
			$senTo = $model->email;
			$body = $model->body;
			$subject = $model->subject;
			$others = $model->others;
			
			if(isset($others) && $others === 'alldownline'){
				$userModel = new Userdb();
				$consultantModel = new Consultant();
				$getAllEmailsOfDownline = $consultantModel -> find()->select('user_id')->where(['in','gen', yii::$app->user->identity->id]) ->asArray() -> all();
				$userId = [];
				foreach($getAllEmailsOfDownline as $value){
					array_push($userId,(int)$value['user_id']);
				}
				//$userId = array_map('intval', $getAllEmailsOfDownline);
				$getAllEmails = $userModel->find()->select('email')->where(['in','id',$userId])->asArray() -> all();
				
			}else if(isset($others) && $others === 'directdownline'){
				$userModel = new Userdb();
				$consultantModel = new Consultant();
				$getAllEmailsOfDownline = $consultantModel -> find()->select('user_id')->where(['father' => yii::$app->user->identity->id]) ->asArray() -> all();
				$userId = [];
				foreach($getAllEmailsOfDownline as $value){
					array_push($userId,(int)$value['user_id']);
				}
				//$userId = array_map('intval', $getAllEmailsOfDownline);
				$getAllEmails = $userModel->find()->select('email')->where(['in','id',$userId])->asArray() -> all();
				
			}else{
				$getAllEmails = null;
				
			}
			
			// if email is greater than one email create a loop and insert them one by one in a new wrole
			if (strpos($model->email, ',') !== false) {
				$explodeEmailString = explode(',',$model->email);
				if(!empty($getAllEmails)){
					$extractedEmail = [];
					foreach($getAllEmails as $value){
						array_push($extractedEmail,$value['email']);
					}
					$getEmail = array_merge($extractedEmail,$explodeEmailString);
				}else{
					$getEmail = $explodeEmailString;
				}
				
				
				foreach($getEmail as $emailValue){
					//$emails = Array($emailValue=>$emailValue);
					Yii::$app->mailqueue->compose(['html' => 'consultantsendmail', 'text' => 'consultantsendmailtext'],
					[
						'email' => $emailValue,
						'body'  => $body,

					])
					->setFrom('from@domain.com')
					->setTo($emailValue)
					->setSubject($subject)
					->setTextBody($body)
					->queue();
					
				}
			} else{
				if(!empty($getAllEmails)){
					$extractedEmail = [];
					foreach($getAllEmails as $value){
						array_push($extractedEmail,$value['email']);
					}
					$getEmail = array_merge($extractedEmail,[$model->email]);
					
					foreach($getEmail as $emailValue){
					//$emails = Array($emailValue=>$emailValue);
					Yii::$app->mailqueue->compose(['html' => 'consultantsendmail', 'text' => 'consultantsendmailtext'],
					[
						'email' => $emailValue,
						'body'  => $body,

					])
					->setFrom('from@domain.com')
					->setTo($emailValue)
					->setSubject($subject)
					->setTextBody($body)
					->queue();
					
				}
					
				}else{
					
					Yii::$app->mailqueue->compose(['html' => 'consultantsendmail', 'text' => 'consultantsendmailtext'],
					[
						'email' => $senTo,
						'body'  => $body,

					])
					->setFrom('from@domain.com')
					->setTo($senTo)
					->setSubject($subject)
					->setTextBody($body)
					->queue();
					
				}
				
			}
			
				// save contact if its not already in the db
			
			if(strpos($senTo, ',') !== false){
				// check if user exists 
				$emails = explode(',',$senTo);
				foreach($emails as $email){
					$newContactModel = new UsersContact();
					$exists = $newContactModel -> find()->where( [ 'email' => $email, 'user_id' => yii::$app->user->identity->id  ] )->exists();
					if(!$exists){
						$newContactModel -> email = $email;
						$newContactModel -> user_id = yii::$app->user->identity->id;
						$newContactModel -> contact_type = 'email';
						$newContactModel ->save(false);
					}
				}
				
			} else{
				$newContactModel = new UsersContact();
				$exists = $newContactModel -> find()->where( [ 'email' => $senTo, 'user_id' => yii::$app->user->identity->id  ] )->exists();
					if(!$exists){
						$newContactModel -> phone_number = $senTo;
						$newContactModel -> user_id = yii::$app->user->identity->id;
						$newContactModel -> contact_type = 'email';
						$newContactModel ->save(false);
					}
				
			}
			
			//return  var_dump($emails);
			//Yii::$app->mailqueue->process();
		}
        
    }
	
	public function actionProcessemail()
    {
			Yii::$app->mailqueue->process();
        
    }

    
}
