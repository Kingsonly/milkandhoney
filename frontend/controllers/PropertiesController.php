<?php
namespace frontend\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use frontend\models\Properties;


/**
 * Site controller
 */
class PropertiesController extends Controller
{
    /**
     * {@inheritdoc}
     */
	
	public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                   
                    [
                        
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
			'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
            
        ];
    }
	

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
		
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
		$model = new Properties();
		$getAllProperties = $model -> find()->where(['status' => 0])->all();
		return $this->render('index',[
			'properties' => $getAllProperties
		]);
        
    }
	
	public function actionPropertyDetails($id)
    {
		$model = new Properties();
		$getPropertyDetail = $model -> find()->where(['property_id'=>$id])->andWhere(['status'=>0])->all();
		return $this->render('proprertydetails',[
			'propertyDetail' => $getPropertyDetail
		]);
        
    }

    
}
