<?php
namespace frontend\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use frontend\models\Consultant;


/**
 * Site controller
 */
class GenerationController extends Controller
{
    /**
     * {@inheritdoc}
     */
	
	public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                   
                    [
                        
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
			'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
            
        ];
    }
	

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
		
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
   public function actionIndex()
    {
		$model = new Consultant();
		$getAllDownline = $model -> find()->where(['status' => 1])->all();
		return $this->render('index',[
			'getAllDownline' => $getAllDownline
		]);
        
    }
	
	public function actionViewgen($id)
    {
		$model = new Consultant();
		$getAllDownline = $model -> find()->where(['status' => 1])->all();
		$getConsultantDetails = $model -> find()->where(['user_id' => $id])->one();
		return $this->render('viewgen',[
			'getAllDownline' => $getAllDownline,
			'getConsultantDetails' => $getConsultantDetails,
			'id' => $id,
		]);
        
    }
	
	public function actionPropertyDetails($id)
    {
		$model = new Properties();
		$getPropertyDetail = $model -> find()->where(['property_id'=>$id])->andWhere(['status'=>0])->all();
		return $this->render('proprertydetails',[
			'propertyDetail' => $getPropertyDetail
		]);
        
    }

    
}
