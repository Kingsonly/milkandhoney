<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "tbl_properties".
 *
 * @property int $property_id
 * @property string $prand
 * @property string $consultant_id
 * @property string $property_name
 * @property string $description
 * @property string $commission_percent
 * @property string $document
 * @property string $photo
 * @property string $file
 * @property string $location
 * @property string $state
 * @property string $price
 * @property string $date
 * @property int $status
 */
class Properties extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_properties';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['prand', 'consultant_id', 'property_name', 'description', 'commission_percent', 'document', 'photo', 'file', 'location', 'state', 'price', 'date', 'status'], 'required'],
            [['property_name', 'description', 'document', 'photo', 'file', 'location', 'state'], 'string'],
            [['price'], 'number'],
            [['date'], 'safe'],
            [['status'], 'integer'],
            [['prand', 'consultant_id'], 'string', 'max' => 25],
            [['commission_percent'], 'string', 'max' => 15],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'property_id' => 'Property ID',
            'prand' => 'Prand',
            'consultant_id' => 'Consultant ID',
            'property_name' => 'Property Name',
            'description' => 'Description',
            'commission_percent' => 'Commission Percent',
            'document' => 'Document',
            'photo' => 'Photo',
            'file' => 'File',
            'location' => 'Location',
            'state' => 'State',
            'price' => 'Price',
            'date' => 'Date',
            'status' => 'Status',
        ];
    }
	
	public function getPropertyViewCount()
    {
        return $this->hasMany(PropertyViewCount::className(), ['property_id' => 'property_id'])->andWhere(['user_id' => Yii::$app->user->identity->id]);
    }
	public function getPropertyAddon()
    {
        return $this->hasMany(PropertyDetails::className(), ['property_id' => 'property_id']);
    }
}
