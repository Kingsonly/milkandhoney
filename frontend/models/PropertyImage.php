<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "tbl_property_image".
 *
 * @property int $id
 * @property int $property_id
 * @property string $image_string
 */
class PropertyImage extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_property_image';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['property_id', 'image_string'], 'required'],
            [['property_id'], 'integer'],
            [['image_string'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'property_id' => 'Property ID',
            'image_string' => 'Image String',
        ];
    }
}
