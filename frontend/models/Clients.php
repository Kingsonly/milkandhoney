<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "tbl_clients".
 *
 * @property int $id
 * @property string $rand
 * @property string $username
 * @property string $pswrd
 * @property string $fname
 * @property string $lname
 * @property string $email
 * @property string $phone
 * @property int $gender
 * @property string $address
 * @property string $city
 * @property string $state
 * @property string $country
 * @property string $date
 * @property int $status
 * @property int $consultant_id
 * @property int $paid
 * @property int $property_id
 */
class Clients extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_clients';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['rand', 'username', 'pswrd', 'fname', 'lname', 'email', 'phone', 'gender', 'address', 'city', 'state', 'country', 'date', 'status', 'consultant_id', 'paid', 'property_id'], 'required'],
            [['pswrd', 'fname', 'lname', 'email', 'phone', 'address', 'city', 'state', 'country'], 'string'],
            [['gender', 'status', 'consultant_id', 'paid', 'property_id'], 'integer'],
            [['date'], 'safe'],
            [['rand'], 'string', 'max' => 25],
            [['username'], 'string', 'max' => 40],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'rand' => 'Rand',
            'username' => 'Username',
            'pswrd' => 'Pswrd',
            'fname' => 'Fname',
            'lname' => 'Lname',
            'email' => 'Email',
            'phone' => 'Phone',
            'gender' => 'Gender',
            'address' => 'Address',
            'city' => 'City',
            'state' => 'State',
            'country' => 'Country',
            'date' => 'Date',
            'status' => 'Status',
            'consultant_id' => 'Consultant ID',
            'paid' => 'Paid',
            'property_id' => 'Property ID',
        ];
    }
	
	public function getUser()
    {
        return $this->hasOne(Userdb::className(), ['id' => 'consultant_id']);
    }
	
	public function getProperties()
    {
        return $this->hasOne(Properties::className(), ['id' => 'property_id']);
    }
	
}
