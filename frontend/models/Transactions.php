<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "tbl_transactions".
 *
 * @property int $id
 * @property string $client_id
 * @property string $property_id
 * @property string $consultant_id
 * @property string $amount_paid
 * @property string $payment_date
 * @property int $payment_method
 * @property string $payment_rand
 * @property int $status
 */
class Transactions extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_transactions';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['client_id', 'property_id', 'consultant_id', 'amount_paid', 'payment_date', 'payment_method', 'payment_rand', 'status'], 'required'],
            [['amount_paid'], 'number'],
            [['payment_date'], 'string'],
            [['payment_method', 'status'], 'integer'],
            [['client_id', 'property_id', 'consultant_id'], 'string', 'max' => 25],
            [['payment_rand'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'client_id' => 'Client ID',
            'property_id' => 'Property ID',
            'consultant_id' => 'Consultant ID',
            'amount_paid' => 'Amount Paid',
            'payment_date' => 'Payment Date',
            'payment_method' => 'Payment Method',
            'payment_rand' => 'Payment Rand',
            'status' => 'Status',
        ];
    }
	
	public function getClient()
    {
        return $this->hasOne(Client::className(), ['id' => 'client_id']);
    }
}
