<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "tbl_message".
 *
 * @property int $id
 * @property string $subject
 * @property string $email_address
 * @property string $group_email
 * @property string $email_content
 * @property string $attachment
 * @property int $status
 * @property string $msg_type
 */
class Message extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_message';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['subject', 'email_contact', 'group_email', 'email_content', 'attachment', 'msg_type'], 'required'],
            [['email_content', 'msg_type'], 'string'],
            [['status'], 'integer'],
            [['subject', 'email_contact', 'attachment'], 'string', 'max' => 250],
            [['group_email'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'subject' => 'Subject',
            'email_contact' => 'Email / Contact',
            'group_email' => 'Group Email',
            'email_content' => 'Email Content',
            'attachment' => 'Attachment',
            'status' => 'Status',
            'msg_type' => 'Msg Type',
        ];
    }
}
