<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "tbl_property_details".
 *
 * @property int $id
 * @property int $property_id
 * @property string $title
 * @property string $description
 * @property string $category
 *
 * @property TblProperties $property
 */
class PropertyDetails extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_property_details';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['property_id', 'title', 'description', 'category'], 'required'],
            [['property_id'], 'integer'],
            [['title', 'description', 'category'], 'string', 'max' => 50],
            [['property_id'], 'exist', 'skipOnError' => true, 'targetClass' => TblProperties::className(), 'targetAttribute' => ['property_id' => 'property_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'property_id' => 'Property ID',
            'title' => 'Title',
            'description' => 'Description',
            'category' => 'Category',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProperty()
    {
        return $this->hasOne(TblProperties::className(), ['property_id' => 'property_id']);
    }
}
