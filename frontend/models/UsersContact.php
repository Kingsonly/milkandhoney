<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "tbl_users_contact".
 *
 * @property int $id
 * @property int $user_id
 * @property string $name
 * @property string $phone_number
 */
class UsersContact extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_users_contact';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'name', 'phone_number'], 'required'],
            [['user_id'], 'integer'],
            [['name','email'], 'string', 'max' => 50],
            [['phone_number'], 'string', 'max' => 11],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'name' => 'Name',
            'phone_number' => 'Phone Number',
            'email' => 'Email',
        ];
    }
}
