<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "tbl_sitecontents".
 *
 * @property int $id
 * @property string $area
 * @property string $title
 * @property string $value
 * @property int $status
 */
class SiteContents extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_sitecontents';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['area', 'title', 'value'], 'required'],
            [['value'], 'string'],
            [['status'], 'integer'],
            [['area'], 'string', 'max' => 50],
            [['title'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'area' => 'Area',
            'title' => 'Title',
            'value' => 'Value',
            'status' => 'Status',
        ];
    }
}
