<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "mail_queue".
 *
 * @property int $id
 * @property string $subject
 * @property string $created_at
 * @property int $attempts
 * @property string $last_attempt_time
 * @property string $sent_time
 * @property string $time_to_send
 * @property string $swift_message
 * @property int $user_id
 */
class MailQueue extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mail_queue';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created_at', 'time_to_send', 'user_id'], 'required'],
            [['created_at', 'last_attempt_time', 'sent_time', 'time_to_send'], 'safe'],
            [['attempts', 'user_id'], 'integer'],
            [['swift_message'], 'string'],
            [['subject'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'subject' => 'Subject',
            'created_at' => 'Created At',
            'attempts' => 'Attempts',
            'last_attempt_time' => 'Last Attempt Time',
            'sent_time' => 'Sent Time',
            'time_to_send' => 'Time To Send',
            'swift_message' => 'Swift Message',
            'user_id' => 'User ID',
        ];
    }
	
	public function getMessageconverted()
	{
		return unserialize(base64_decode($this->swift_message));
	}
}
