<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "tbl_commissions".
 *
 * @property int $id
 * @property string $property_id
 * @property string $commission_paid
 * @property string $percentage
 * @property string $payment_date
 * @property string $payment_rand
 * @property int $status
 * @property int $consultant_id
 * @property int $client_id
 */
class Commissions extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_commissions';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['property_id', 'commission_paid', 'percentage', 'payment_date', 'payment_rand', 'status', 'consultant_id', 'client_id'], 'required'],
            [['commission_paid'], 'number'],
            [['payment_date'], 'safe'],
            [['status', 'consultant_id', 'client_id'], 'integer'],
            [['property_id'], 'string', 'max' => 25],
            [['percentage'], 'string', 'max' => 50],
            [['payment_rand'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'property_id' => 'Property ID',
            'commission_paid' => 'Commission Paid',
            'percentage' => 'Percentage',
            'payment_date' => 'Payment Date',
            'payment_rand' => 'Payment Rand',
            'status' => 'Status',
            'consultant_id' => 'Consultant ID',
            'client_id' => 'Client ID',
        ];
    }
}
