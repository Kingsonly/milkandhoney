<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "tbl_property_view_count".
 *
 * @property int $id
 * @property int $user_id
 * @property int $property_id
 * @property int $views
 */
class PropertyViewCount extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_property_view_count';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'property_id'], 'required'],
            [['user_id', 'property_id', 'views'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'property_id' => 'Property ID',
            'views' => 'Views',
        ];
    }
}
