<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "tbl_consultant".
 *
 * @property int $id
 * @property int $user_id
 * @property string $f_name
 * @property string $l_name
 * @property string $dob
 * @property string $phones
 * @property string $pic_path
 * @property string $ref_code
 * @property string $country
 * @property string $state
 * @property string $city
 * @property string $address
 * @property string $gen
 * @property string $bank
 * @property string $acc_name
 * @property string $acc_num
 * @property double $in_account
 * @property string $withdraws
 * @property int $status
 * @property int $gender
 * @property string $father
 * @property int $activate
 * @property int $sms_status
 * @property int $email_status
 */
class Consultant extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_consultant';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'f_name', 'l_name', 'dob', 'phones', 'pic_path', 'ref_code', 'country', 'state', 'city', 'address', 'gen', 'bank', 'acc_name', 'acc_num', 'in_account', 'withdraws', 'gender', 'father'], 'required'],
            [['user_id', 'status', 'gender', 'activate', 'sms_status', 'email_status'], 'integer'],
            [['dob'], 'safe'],
            [['country', 'state', 'city', 'address'], 'string'],
            [['in_account'], 'number'],
            [['f_name', 'l_name', 'pic_path', 'gen', 'bank', 'acc_name'], 'string', 'max' => 255],
            [['phones'], 'string', 'max' => 13],
            [['ref_code'], 'string', 'max' => 25],
            [['acc_num'], 'string', 'max' => 12],
            [['withdraws', 'father'], 'string', 'max' => 11],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'f_name' => 'F Name',
            'l_name' => 'L Name',
            'dob' => 'Dob',
            'phones' => 'Phones',
            'pic_path' => 'Pic Path',
            'ref_code' => 'Ref Code',
            'country' => 'Country',
            'state' => 'State',
            'city' => 'City',
            'address' => 'Address',
            'gen' => 'Gen',
            'bank' => 'Bank',
            'acc_name' => 'Acc Name',
            'acc_num' => 'Acc Num',
            'in_account' => 'In Account',
            'withdraws' => 'Withdraws',
            'status' => 'Status',
            'gender' => 'Gender',
            'father' => 'Father',
            'activate' => 'Activate',
            'sms_status' => 'Sms Status',
            'email_status' => 'Email Status',
        ];
    }
	
	public function getUser()
    {
        return $this->hasOne(Userdb::className(), ['id' => 'user_id']);
    }
	
}
