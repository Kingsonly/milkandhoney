<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "tbl_sitecontents_faq".
 *
 * @property int $id
 * @property string $area
 * @property string $title
 * @property string $icon
 * @property string $value
 * @property string $more details
 * @property int $status
 */
class SitecontentsFaq extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_sitecontents_faq';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['area', 'title', 'icon', 'value', 'more details'], 'required'],
            [['value', 'more details'], 'string'],
            [['status'], 'integer'],
            [['area', 'title'], 'string', 'max' => 100],
            [['icon'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'area' => 'Area',
            'title' => 'Title',
            'icon' => 'Icon',
            'value' => 'Value',
            'more details' => 'More Details',
            'status' => 'Status',
        ];
    }
}
