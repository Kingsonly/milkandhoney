<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "tbl_consoltant_transactions".
 *
 * @property int $id
 * @property int $consultants_id
 * @property int $amount
 * @property string $date
 * @property int $type
 * @property int $status
 */
class ConsoltantTransactions extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_consoltant_transactions';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['consultants_id', 'amount', 'date', 'type', 'status'], 'required'],
            [['consultants_id', 'amount', 'type', 'status'], 'integer'],
            [['date'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'consultants_id' => 'Consultants ID',
            'amount' => 'Amount',
            'date' => 'Date',
            'type' => 'Type',
            'status' => 'Status',
        ];
    }
}
