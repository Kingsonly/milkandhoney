<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class SiteAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        
        "uniland/css/bootstrap.css",
        "uniland/css/style.css",
		"uniland/css/font-awesome.css",
		"uniland/fonts/flaticon.css",
		"uniland/css/color.css",
		"uniland/css/jslider.css",
		"uniland/css/responsive.css",
		"uniland/css/loader.css"
    ];
    public $js = [
		"uniland/js/jquery.min.js",
		"uniland/js/bootstrap.min.js",
		"uniland/js/bootstrap-select.js",
		"uniland/js/YouTubePopUp.jquery.js",
		"uniland/js/jquery.fancybox.pack.js",
		"uniland/js/jquery.fancybox-media.js",
		"uniland/js/owl.js",
		"uniland/js/mixitup.js",
		"uniland/js/wow.js",
		"uniland/js/jshashtable-2.1_src.js",
		"uniland/js/jquery.numberformatter-1.2.3.js",
		"uniland/js/tmpl.js",
		"uniland/js/jquery.dependClass-0.1.js",
		"uniland/js/draggable-0.1.js", 
		"uniland/js/jquery.slider.js", 
		"uniland/js/custom.js",
	
    ];
    public $depends = [
        //'yii\web\YiiAsset',
        //'yii\bootstrap\BootstrapAsset',
    ];
}

 