<?
namespace frontend\components;


use Yii;
use yii\base\Component;
use frontend\models\SiteContents;
use frontend\models\SitecontentsService;
use frontend\models\SitecontentsFaq;
use frontend\models\SitecontentsAboutus;
use frontend\models\Properties;
use yii\base\InvalidConfigException;
 
class MySiteComponent extends Component
{
	/*
	 * this component is used to display contetnts on the main website 
	 * this would reduce making configs directly on the controllers, and if model changes, this file would * be responsible to handle the new change 
	 */
	
	/*
	 * getContent function is used to get all the database contetnts for the site/index page asuch modifications to this method would simply implie a change on the site home page 
	 */
	
	public function getContent($area)
	{
		$model = new SiteContents();
		$getContent = $model->find()->where(['area' => $area])->all();
	 	return $getContent;
	}
	
	public function getProperties()
	{
		$model = new Properties();
		$getAllProperties = $model -> find()->where(['status' => 0])->all();
		return $getAllProperties;
	}
	
	public function getSingleServiceContent($area)
	{
		$model = new SitecontentsService();
		$getAllProperties = $model -> find()->where(['area' => $area])->one();
		return $getAllProperties;
	}
	
	public function getServiceContent($area)
	{
		$model = new SitecontentsService();
		$getSiteContent = $model->find()->where(['area' => $area])->all();
	 	return $getSiteContent;
	}
	
	public function getSingleServiceContentFaq($area)
	{
		$model = new SitecontentsFaq();
		$getSingleContent = $model -> find()->where(['area' => $area])->one();
		return $getSingleContent;
	}
	
	public function getServiceContentFaq($area)
	{
		$model = new SitecontentsFaq();
		$getSiteContent = $model->find()->where(['area' => $area])->all();
	 	return $getSiteContent;
	}
	
	public function getSingleServiceContentAboutus($area)
	{
		$model = new SitecontentsAboutus();
		$getSingleContent = $model -> find()->where(['area' => $area])->one();
		return $getSingleContent;
	}
	
	public function getServiceContentAboutus($area)
	{
		$model = new SitecontentsAboutus();
		$getSiteContent = $model->find()->where(['area' => $area])->all();
	 	return $getSiteContent;
	}
 
}